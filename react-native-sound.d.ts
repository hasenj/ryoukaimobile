declare module 'react-native-sound' {
    export = class Sound {

        static CACHES: string;
        static MAIN_BUNDLE: string;
        static DOCUMENT: string;
        static LIBRARY: string;

        /**
            @param filename {string} Either absolute or relative path to the sound file

            @param basePath {?string} Optional base path of the file. Omit this or pass '' if filename is an absolute path. Otherwise, you may use one of the predefined directories: Sound.MAIN_BUNDLE, Sound.DOCUMENT, Sound.LIBRARY, Sound.CACHES.

            @param callback Optional callback function. If the file is successfully loaded, the first parameter error is null, and props contains an object with two properties: duration (in seconds) and numberOfChannels (1 for mono and 2 for stereo sound), both of which can also be accessed from the Sound instance object. If an initialization error is encountered (e.g. file not found), error will be an object containing code, description, and the stack trace.
         */
        constructor(filename: string, basePath?: string, callback?: (error: Error, props: { duration: number, numberOfChannels: number }) => void);

        /** Return true if the sound has been loaded. */
        isLoaded(): boolean;

        /** @param onEnd: Optinoal callback function that gets called when the playback finishes successfully or an audio decoding error interrupts it. */
        play(onEnd?: (successfully: boolean) => void): void;

        /** Pause the sound. */
        pause(): void;
        /** Stop the playback. */
        stop(): void;

        /** Release the audio player resource associated with the instance. */
        release(): void;

        /** Return the duration in seconds, or -1 before the sound gets loaded. */
        getDuration(): number;

        /** Return the number of channels (1 for mono and 2 for stereo sound), or -1 before the sound gets loaded. */
        getNumberOfChannels(): number;

        /**
            Return the volume of the audio player (not the system-wide volume),
            ranging from 0.0 (silence) through 1.0 (full volume, the default).
        */
        getVolume(): number;

        /** Set the volume, ranging from 0.0 (silence) through 1.0 (full volume). */
        setVolume(value: number): void;

        /**
            Return the stereo pan position of the audio player (not the system-wide pan),
            ranging from -1.0 (full left) through 1.0 (full right). The default value is 0.0 (center).
        */
        getPan(): number;

        /** Set the pan, ranging from -1.0 (full left) through 1.0 (full right). */
        setPan(value: number): void;

        /**
            Return the loop count of the audio player.

            The default is 0 which means to play the sound once.
            A positive number specifies the number of times to return to the start and play again.
            A negative number indicates an indefinite loop.
        */
        getNumberOfLoops(): number;

        /**
            Set the loop count.
            @param value:
                0 means to play the sound once.
                A positive number specifies the number of times to return to the start and play again (iOS only).
                A negative number indicates an indefinite loop (iOS and Android).
         */
        setNumberOfLoops(value: number): void;

        /**
            @param callback: will receive the current playback position in seconds and whether the sound is being played.
        */
        getCurrentTime(callback: (seconds: number, isPlaying: boolean) => void): void;

        /**
            Seek to a particular playback point in seconds.
         */
        setCurrentTime(value: number): void;
    }
}