/** This module is for parsing the arabic meaning entry of a word */

import u = require('lodash');

export abstract class Segment {}

export abstract class BasicTextSegment extends Segment {
    constructor(public text: string) {
        super();
    }
}

/** A regular text segment; forms the main part of the meaning */
export class TextSegment extends BasicTextSegment {}

/** A faded comment to give some context to explanation. Usually inside a parenthesis */
export class CommentSegment extends BasicTextSegment {}

/** Another way to give some context, but this one is green and inside { curly braces }. Usually before the meaning. */
export class GreenCommentSegment extends BasicTextSegment {}

// type LinkType = "A" | "S" | "M" | "B";
type LinkType = string;

/** A segment that should be pressable and lead somewhere */
export abstract class LinkSegment extends BasicTextSegment {}

/** Links to another word; usually by ID */
export class WordLink extends LinkSegment {
    constructor(text: string, public link: string | number, public type?: LinkType) {
        super(text);
    }
}

/** A link to an image (by filename; without path or extension) */
export class ImageLink extends LinkSegment {
    constructor(public fileName: string) {
        super("");
    }
}

/** A non-link. Not sure why it's there. */
export class FakeLink extends LinkSegment {}

export abstract class ContainerSegment extends Segment {
    constructor(public segments: Segment[]) {
        super();
    }
}

/** A note follows the meaning and explains subtelties with it. */
export class NoteSegment extends ContainerSegment {}

/** A way to encapsulate multiple sub-segments. For example, to put a note that doesn't extend until the end of the line */
export class SquareBracket extends ContainerSegment { }

class TextIterator {
    index = 0;
    constructor(public text: string) {
    }
    peek(len = 1) {
        return this.text.substr(this.index, len);
    }
    done() {
        return this.index >= this.text.length;
    }
    indexOfNext(str: string) {
        return this.text.indexOf(str, this.index);
    }
}

function nextPart(it: TextIterator, position: number): Segment {
    if (it.peek() == '[') { // sub segment!
        let start = it.index + 1;
        let end = it.indexOfNext(']'); // for now ok since [] does not nest (I think)
        let segment = it.text.substring(start, end);
        it.index = end + 1;
        return new SquareBracket(parseMeaning(segment));
    }
    // faded explanations!!
    if (it.peek() == '|') {
        it.index++;
        let start = it.index;
        let end = it.indexOfNext('|');
        it.index = end + 1;
        let segment = it.text.substring(start, end)
        return new CommentSegment(segment)
    }
    // links!
    if (it.peek() == '{') {
        let start = it.index + 1;
        let end = it.indexOfNext('}');
        it.index = end + 1;
        let segment = it.text.substring(start, end);
        if (segment.indexOf(':') == -1) {
            // not sure why but {text} are shown as green on the website
            // segment = '{' + segment + '}'
            return new GreenCommentSegment(segment);
        } else {
            let [kanji, idstr, type] = segment.split(':'); // two or three parts; type is optional
            if (idstr == 'anchor') {
                // the first part is the link, not the kanji
                // but sometimes it itself has two parts
                // if it's one part, it's the picture filename
                // if it' has two prts, the first part is the word id and
                // the second part is the picture filename
                let [part1, part2] = kanji.split("#");
                let filename = part1;
                if(part2) {
                    filename = part2;
                }
                return new ImageLink(filename);
            }
            if (idstr == 'no') {
                // normal text with no link and is not in green!
                return new FakeLink(kanji);
            }
            let id = parseInt(idstr); // parseInt will tolerate the #xx part of yyyy#xx
            let linkQuery: string|number = kanji;
            if(isFinite(id)) {
                linkQuery = id;
            }
            // console.log("New word link!", kanji, linkQuery, type);
            return new WordLink(kanji, linkQuery, type);
        }
    }

    // end of line or some kind of separator that ends up being ignored
    if (it.peek() == '؛') {
        it.index++;
        return null;
    }

    if (it.peek() == '$') {
        let start = it.index + 1;
        let end = it.text.indexOf('$', start);
        if(end == -1) {
            end = it.text.length;
        }
        it.index = end;
        let segment = it.text.substring(start, end);
        return new NoteSegment(parseMeaning(segment));
    }

    // now finally we come to treaing the normal text!!
    let start = it.index;
    let end = start;
    let endChar = '';
    // find the next symbol
    while (!it.done()) {
        it.index++;
        let chr = it.peek();
        // special segment end chars!
        if (chr == ']' || chr == '[' || chr == '{' || chr == '|' || chr == '$' || chr === '؛') {
            endChar = chr;
            break;
        }
    }
    end = it.index;
    let segment = it.text.substring(start, end);
    // special case HACK; text following by a note should have a semicolon (explanetory) before the note
    if(endChar == '$') {
        segment += ':';
    }
    return new TextSegment(segment);
}

export function parseMeaning(text: string): Segment[] {
    text = u.trim(text);
    let out: Segment[] = [];
    let it = new TextIterator(text);
    skipObsoleteLink(it);
    while (!it.done()) {
        let next = nextPart(it, out.length);
        if (next) { // ignore nulls (because sometimes it returns a null)
            out.push(next);
        }
    }
    return out;
}

// only at the beginning!
function skipObsoleteLink(it: TextIterator) {
    if (it.index == 0) {
        let r = /[0-9#*]/;
        while (r.test(it.peek()) || it.peek() == '،') {
            it.index++;
        }
    }
}
