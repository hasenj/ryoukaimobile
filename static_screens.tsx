import React = require('react');
import ReactNative = require('react');
import { Component } from 'react';
import {
    AppRegistry,
    View, ScrollView, Text, Image, TextInput, ActivityIndicator
} from 'react-native';

export function LoadingScreen() {
    return <View style={{
        flex: 1, backgroundColor: 'whitesmoke', borderRadius: 10,
        right: 0, left: 0, top: 0, bottom: 0
    }}>
        <Text style={{
            fontSize: 20, textAlign: 'center', fontFamily: 'Verdana',
            color: 'hsl(220, 60%, 26%)', padding: 20, marginTop: 30,
            writingDirection: 'rtl'
        }}>
            يرجى الانتظار قليلا ... {'\n'}
            نقوم الآن بتحميل القاموس
        </Text>
        <ActivityIndicator animating={true} size='large'
            style={{
                height: 80, alignItems: 'center',
                justifyContent: 'center',
                padding: 8,
            }} />
    </View>
}

export function ErrorScreen(text: string) {
    return <View style={{
        flex: 1, backgroundColor: 'red',
        right: 0, left: 0, top: 0, bottom: 0,
    }}>
        <Text style={{
            fontSize: 18, fontWeight: 'bold',
            color: 'white', marginTop: 30, padding: 20,
            textAlign: 'center', writingDirection: 'rtl'
        }}>
            { text }
        </Text>
    </View>

}