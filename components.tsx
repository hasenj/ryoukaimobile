import u = require('lodash');
import React = require('react');
import ReactNative = require('react-native');
import { Component } from 'react';
import {
    AppRegistry, Modal,
    Animated, LayoutAnimation, WebView, ActivityIndicator,
    View, ScrollView, ListView, Text, Image, TextInput, Switch,
    TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback,
    TextStyle, ViewStyle
} from 'react-native';

import Styles = require("./styles");

import dict = require('./dict');

import meaning = require("./meaning_parser");

import { MainNavigator } from "./nav";

interface PageProps {
    navigator: MainNavigator;
}
interface PageState {
}

interface WordEntryProps {
    entry: dict.WordEntry;
    hostPage: PageWithLink;
    autoPlaySound?: boolean; // upon expanding for the first time, automatically play sound!
}

interface WordEntryState {
}

interface SwitchLabelProps extends ReactNative.SwitchProperties {
    label: string;
    viewStyle?: ViewStyle;
    labelStyle?: TextStyle;
}

function ArabicSummary(props: { summaries: string[] }) {
    let text = props.summaries.join(' / ')
    return <Text key='summary' numberOfLines={2} style={Styles.resultAR}> { text } </Text>
}

function segmentsToElements(segments: meaning.Segment[], keyPrefix: string, page: PageWithLink): JSX.Element[] {
    return segments.map((s, i) => segmentToElement(s, keyPrefix + i, page));
}

function segmentToElement(segment: meaning.Segment, key: string, page: PageWithLink): JSX.Element {
    // console.log("key:", key, segment.constructor.name);
    if (segment instanceof meaning.TextSegment) {
        return <Text key={key}>{ segment.text }</Text>
    }
    if (segment instanceof meaning.CommentSegment) {
        return <Text key={key} style={{ color: 'gray' }}>{ segment.text }</Text>
    }
    if (segment instanceof meaning.GreenCommentSegment) {
        let text = '{' + segment.text + '}'
        return <Text key={key} style={{ color: 'green' }}>{ text }</Text>
    }
    if (segment instanceof meaning.FakeLink) {
        return <Text key={key} style={{ color: 'green', paddingHorizontal: 4 }}>{ segment.text }: </Text>
    }
    if (segment instanceof meaning.ImageLink) {
        return <Text key={key} onPress={jumpToImageByFileName.bind(null, page, segment.fileName) }> { ImageIcon() } </Text>
    }
    if (segment instanceof meaning.WordLink) {
        // console.log("Word link:", segment);
        let linkIcon: JSX.Element = null;
        if (segment.type) {
            // console.log("Type:", segment.type)
            let iconName = "arr" + segment.type.toUpperCase();
            let src = ({ arrA, arrS, arrM, arrB } as any)[iconName]
            if (src) {
                linkIcon = <Image key="icon" source={src} resizeMode="contain" style={{ height: 16, width: 26 }}/>
            }
        }
        // clickable link finds the word!
        return <Text key={key}>
            {rlm}{ linkIcon }{rlm}
            <Text key="link" style={{ color: 'blue', paddingHorizontal: 4, borderRadius: 4 }}
                onPress={jumpToWordSearch.bind(null, page, segment.link) }>{lrm}{ segment.text }{lrm}</Text>
        </Text>
    }
    if (segment instanceof meaning.NoteSegment) {
        return <Text key={key}>{ segmentsToElements(segment.segments, `${key}_`, page) }</Text>
    }
    /*
    if (segment instanceof meaning.NotesSegment) {
        return <Text key={key}>
            <Text key="chuui" style={{ color: "red" }}>注: </Text>
            <Text key="notes">
                { segment.notes.map((note, i) => {
                    return <Text key={i}>
                        <Text style={{ color: "red" }}>{'\n'}※ </Text>
                        <Text style={{ color: "green" }}>{ segmentsToElements(note.segments, `${key}_${i}`, page) }</Text>
                    </Text>
                }) }
            </Text>
        </Text>
    }
    */
    if (segment instanceof meaning.SquareBracket) {
        return <Text key={key}>[
            { segmentsToElements(segment.segments, `${key}_`, page) }
            ]</Text>
    }

    console.warn("Failed to handle segment:", segment.constructor.name, segment)
    return <Text key={key}>???</Text>
}

function FullArabicMeaning(props: { entry: dict.WordEntry, hostPage: PageWithLink }) {
    let entries = props.entry.arabicDetails;
    let meaningEntries: JSX.Element[] = [];
    const styles = [Styles.resultAR, { marginBottom: 10, lineHeight: 20 }];
    let meaningLines: meaning.Segment[][] = entries.map(meaning.parseMeaning);
    // separate the end notes into a separate list!
    let noteLines: meaning.Segment[][] = [];
    for(let i = 0; i < meaningLines.length; i++) {
        let segments = meaningLines[i];
        if(segments[0] instanceof meaning.NoteSegment) {
            noteLines = meaningLines.slice(i);
            meaningLines = meaningLines.slice(0, i);
        }
    }
    for(let m_index = 0; m_index < meaningLines.length; m_index++) {
        let segments = meaningLines[m_index];
        let prefix: JSX.Element = null;
        if (meaningLines.length > 1) {
            prefix = <Text style={Styles.meaningNumber}> ({m_index + 1}) </Text>;
        }
        let textParts = segmentsToElements(segments, m_index + "_", props.hostPage);
        meaningEntries.push(<Text key={m_index} style={styles}>
            { prefix }
            { textParts }
        </Text>);
    }

    let notesElement: JSX.Element = null;
    if(noteLines.length > 0) {
        let notes: meaning.NoteSegment[] = [];
        for(let line of noteLines) {
            notes.push(...(line as meaning.NoteSegment[]));
        }
        notesElement = <Text key="end_notes" style={styles}>
            <Text key="chuui" style={{ color: "red", writingDirection: "ltr" }}>注: </Text>
            <Text key="notes">
                { notes.map((note, i) => {
                    return <Text key={i}>
                        <Text style={{ color: "red" }}>{'\n'}※ </Text>
                        <Text style={{ color: "green" }}>{ segmentsToElements(note.segments, `end_notes_${i}`, props.hostPage) }</Text>
                    </Text>
                }) }
            </Text>
        </Text>
    }
    return <View>
        { meaningEntries }
        { notesElement }
    </View>
}
function extractSummary(arabicMeaning: string): string {
    // console.log("Extracting summary from:", arabicMeaning);
    let result = meaning.parseMeaning(arabicMeaning);
    function extractText(segment: meaning.Segment) {
        if(segment instanceof meaning.TextSegment) {
            return segment.text;
        }
        if(segment instanceof meaning.WordLink) {
            return segment.text;
        }
        return "";
    }
    function cleanupLine(line: string): string {
        // remove parenthesis and empty spaces between commas
        return line.replace(/\([^)]*\)/g, "")
                .replace(/\s+/g, " ")
                .split("،")
                    .map(s => s.trim())
                    .filter(e => !!e.trim())
                    .join("، ").trim();
    }
    let summary = result.map(extractText).join("");
    return cleanupLine(summary);
}

function JapaneseWord(entry: dict.WordEntry) {
    let kanjis: JSX.Element[] = null;
    if (entry.kanji.length > 0) {
        kanjis = entry.kanji.map((k, k_index) => {
            let styles = [Styles.resultJP];
            if (k.outdated) {
                styles.push(Styles.resultOutdated);
            }
            let last = k_index == entry.kanji.length - 1;
            let postfix = last ? null : <Text style={{ color: 'brown' }}>・</Text>;
            return <Text key={k_index}>
                <Text style={styles}> {k.kanji} </Text>
                { postfix }
            </Text>
        })
    }
    return <Text style={Styles.resultJP}>
        <Text>{entry.kana}</Text>
        {' '}
        <Text style={{ fontWeight: 'bold', color: 'gray' }}>
            【
        </Text>
            { kanjis }
        <Text style={{ fontWeight: 'bold', color: 'gray' }}>
            】
        </Text>
    </Text>
}

// get the first kanji version of the word that is not outdated; fall back to kana
function getWordKanji(word: dict.WordEntry) {
    for(let k of word.kanji) {
        if(!k.outdated) {
            return k.kanji;
        }
    }
    return word.kana;
}

function WordImage (props: dict.WordImageProps) {
    let centerChildren: ViewStyle = { justifyContent: 'center', alignItems: 'center' }
    return <View style={[{ marginBottom: 20 }, Styles.borderTop(Styles.blueishBorder), Styles.borderBottom(Styles.blueishBorder)]}>
    <View style={[{ padding: 5, backgroundColor: Styles.blueishBG }, centerChildren]}>
        <Text style={{ fontSize: 14, color: Styles.blueishBorder }}>{ props.description }</Text>
    </View>
    <View style={centerChildren}>
        <Image source={{ uri: props.uri }}
            style={{ width: props.width, height: props.height }}
            resizeMode="contain" />
    </View>
    </View>
}

function WordImages(props: {pictures: dict.WordImageProps[]}) {
    return <View>
        {props.pictures.map( (p, i) => <WordImage key={i} {...p}/>)}
    </View>
}

interface WordImagePageProps extends dict.WordImageProps, PageProps {

}

export class WordImagePage extends Component<WordImagePageProps, PageState> {
    constructor(props: WordImagePageProps) {
        super(props);
    }
    render(): JSX.Element {
        return <View style={Styles.pageStyle}>
            <Navbar hostPage={this}/>
            <WordImage {...this.props}/>
        </View>
    }
}

function _ExampleSentence(example: dict.ExampleEntry): JSX.Element {
    // console.log("Rendering Example");
    return <View style={[Styles.borderBottom("gray"), { padding: 10}]}>
        <View><Text style={{fontSize: 14}}> { example.text } </Text></View>
        <View><Text style={{fontSize: 10}}> { example.reading } </Text></View>
        <View><Text style={{fontSize: 14, writingDirection: 'rtl', textAlign: 'right'}}> { example.arabic } </Text></View>
    </View>
}

export interface ExampleSentenceProps {
    example: dict.ExampleEntry;
    index: number;
}

export interface ExampleSentenceState {

}

import overfit = require("./overfit");
import { WebContainer } from "./web-container";

export class ExampleSentence extends Component<ExampleSentenceProps, ExampleSentenceState> {
    html: string; // ruby-fied html text
    constructor(props: ExampleSentenceProps) {
        super(props);
        try {
            this.html = overfit.overfitToRuby(props.example.text, props.example.reading);
        } catch (e) {
            console.log(props.example)
            console.log(e);
            // debugger;
            this.html = `<div>${props.example.text}</div><div>${props.example.reading}</div>`;
        }
    }

    render() {
        let example = this.props.example;
        let sentenceHtml = this.html;
        let styles: ViewStyle[] = [{ padding: 10 }];
        if(this.props.index !== 0) {
            styles.push(Styles.borderTop("gray"));
        }
        return <View style={styles}>
            <WebContainer html={sentenceHtml} style=""/>
            <View><Text style={{ fontSize: 14, writingDirection: 'rtl', textAlign: 'right' }}> { example.arabic } </Text></View>
        </View>
    }
}

import Sound = require('react-native-sound');

export async function playSound(soundFileName: string) {
    let file = `www/wordsSounds/${soundFileName}.mp3`;
    // console.log("Attempting to play:", soundFileName);
    return new Promise((resolve, reject) => {
        let s = new Sound(file, Sound.MAIN_BUNDLE, (e, p) => {
            if (e) {
                reject(e);
            } else {
                s.play((ok) => {
                    s.release();
                    if (ok) {
                        resolve();
                    } else {
                        reject(new Error(`Playing ${soundFileName} failed!`));
                    }
                });
            }
        })
    });
}

function safePlayAudio(soundFileName: string) {
    playSound(soundFileName).catch(e => {
        console.log("play sound failed!", e);
    });
}

function pluralizeExamplesArabic(n: number) {
    if (n <= 0) {
        return "";
    }
    if (n == 1) {
        return "مثال";
    }
    if (n == 2) {
        return "مثالين";
    }
    if (n >= 3 && n <= 10) {
        return "أمثلة";
    }
    if (n > 10) {
        return "مثال"
    }
}

export const rlm = String.fromCharCode(0x200f);
export const lrm = String.fromCharCode(0x200e);

function BackButton(navigator: MainNavigator): JSX.Element {
    let onPress = () => {
        navigator.back();
    }
    return <Button onPress={onPress} iconElement={ BackIcon() }/>
}

function SearchButton(navigator: MainNavigator): JSX.Element {
    let jumpToSearchPage = () => {
        navigator.push(UserDrivenSearch, { navigator })
    }
    return <Button iconElement={SearchIcon()} onPress={jumpToSearchPage}/>
}

function jumpToPreferencesPage(navigator: MainNavigator) {
    let props: PageProps = {
        navigator: navigator
    }
    navigator.push(PreferencesPage, props)
}

/*
function PreferencesButton(navigator: MainNavigator): JSX.Element {
    return <Button iconElement={ CogIcon() } onPress={jumpToPreferencesPage.bind(null, navigator)} />
}
*/

function Button(props: {text?: string, iconElement?: JSX.Element, onPress?: () => void}): JSX.Element {
    let iconElement: JSX.Element = null;
    let textElement: JSX.Element = null;
    if(props.text) {
        textElement = <Text style={{ writingDirection: 'rtl' }}>{ props.text }</Text>
    }
    if(props.iconElement) {
        iconElement = props.iconElement;
    }
    let style = [Styles.buttonBorder, Styles.buttonShape];
    let opacity = 1;
    if(!props.onPress) {
        opacity = 0.5
    }
    return <TouchableHighlight underlayColor={Styles.underlayColor}
        onPress={props.onPress}
        style={style}>
        <View style={{ flexDirection: "row-reverse", opacity }}>
            { iconElement }
            { textElement }
        </View>
    </TouchableHighlight>
}

export class WordEntry extends Component<WordEntryProps, WordEntryState> {
    constructor(props: WordEntryProps) {
        super(props);
        this.state = {
            summaryMode: true
        };
        this.buildSummary(props);
    }

    arabicSummaries: string[] = [];
    componentWillReceiveProps(props: WordEntryProps) {
        this.buildSummary(props);
    }

    buildSummary(props: WordEntryProps) {
        // filter out empty lines
        this.arabicSummaries = props.entry.arabicDetails.map(extractSummary).filter(e => !!e.trim());
    }

    jumpToWordPage() {
        jumpToWordPage(this.props.hostPage, this.props.entry);
    }

    render() {
        let entry = this.props.entry;
        let meanings = <ArabicSummary summaries={this.arabicSummaries}/>
        let backgroundColor = 'white';
        let examplesCount = "";
        if (entry.examples.length) {
            examplesCount = `${entry.examples.length} ${pluralizeExamplesArabic(entry.examples.length)}`;
        }
        let examplesSummary = `${examplesCount ? `${rlm}(${examplesCount})${rlm}` : ''}`
        let soundIcon: JSX.Element = null;
        let pictureIcon: JSX.Element = null;
        let examplesIcon: JSX.Element = null;
        if (entry.soundFileName) {
            soundIcon = <Text> {SpeakerIcon()} </Text>
        }
        if(entry.pictures && entry.pictures.length) {
            pictureIcon = <Text> {ImageIcon()} </Text>
        }
        if(entry.examples && entry.examples.length) {
            examplesIcon = <Text> {BubbleIcon()} </Text>
        }
        return <TouchableHighlight underlayColor={Styles.underlayColor} delayPressIn={150}
            style={[Styles.borderBottom("gray"), { padding: 10, backgroundColor: backgroundColor }]}
            onPress={this.jumpToWordPage.bind(this) }>
            <View style={{ padding: 10 }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                    <JapaneseWord {...entry}/>
                    { examplesIcon }
                    { pictureIcon }
                    { soundIcon }
                    {/* <Text> { examplesSummary } </Text> */}
                </View>
                <View>
                    { meanings }
                </View>
            </View>
        </TouchableHighlight>
    }
}


const wordsDs = new ListView.DataSource({ rowHasChanged: (r1: dict.WordEntry, r2: dict.WordEntry) => r1.id !== r2.id })
const exampleDs = new ListView.DataSource({ rowHasChanged: (r1: dict.ExampleEntry, r2: dict.ExampleEntry) => r1.id !== r2.id })

interface SearchBoxState {
    initialValue: string;
    currentValue: string;
}
interface SearchBoxProps {
    hostPage: PageWithLink;
    navbar?: Navbar;
}
type RNTextEvent = { nativeEvent: { text: string } };
class SearchBox extends Component<SearchBoxProps, SearchBoxState> {
    private static lastSearch: string;

    constructor(props: SearchBoxProps) {
        super(props);
        this.state = {
            initialValue: SearchBox.lastSearch,
            currentValue: SearchBox.lastSearch
        }
    }

    async onSubmit(event: RNTextEvent) {
        let term = event.nativeEvent.text;
        SearchBox.lastSearch = term; // remember last term!
        await jumpToWordSearch(this.props.hostPage, term);
        let navbar = this.props.navbar;
        if(navbar) {
            if(!navbar.props.defaultSearchOn) {
                // timeout so it happens after transition starts
                // NOTE: not sure if this can cause a warning due to setting state of something that is not mounted anymore!
                setTimeout(() => navbar.toggleSearch());
            }
        }
    }

    async onChange(term: string) {
        this.state.currentValue = term;
        this.setState(this.state);
    }

    async onClear() {
        let navbar = this.props.navbar;
        if (this.state.currentValue) {
            ((this.refs as any).searchBox as TextInput).clear();
            this.state.currentValue = "";
            SearchBox.lastSearch = "";
            this.setState(this.state);
        } else {
            if (navbar) {
                navbar.toggleSearch(false);
            }
        }
    }

    render() {
        let paddingRight = { paddingRight: 30 };
        let closeButton = <TouchableOpacity
            style={{ position: "absolute", right: 20, top: 14 }}
            onPress={this.onClear.bind(this) }>
            <View style={{ opacity: 0.3 }}>{ CancelIcon() }</View>
        </TouchableOpacity>

        return <View style={{ flex: 1 }}>
            <TextInput returnKeyType='search'
                autoCapitalize='none' autoCorrect={false} autoFocus={true}
                style={[{
                    flex: 1,
                    marginRight: 10,
                    height: 40,
                    paddingHorizontal: 14, borderRadius: 6,
                    backgroundColor: 'whitesmoke'
                }, paddingRight]}
                placeholder='البحث'
                defaultValue={this.state.initialValue}
                ref="searchBox"
                onChangeText={this.onChange.bind(this) }
                onSubmitEditing={this.onSubmit.bind(this) }/>
            { closeButton }
        </View>
    }
}

interface UserSearchProps extends PageProps {
}

interface UserSearchState extends PageState {
    term: string;
}

// this is basically the home page!
export class UserDrivenSearch extends Component<UserSearchProps, UserSearchState> {
    listview: ScrollView = null; // should be ListView but the type definition is missing the scrollTo definition
    currentSearchTerm: string;

    constructor(props: UserSearchProps) {
        super(props);
        this.state = {
            term: ""
        };
        // console.log("Initializing word searching; navigator:", props.navigator);
        // TEST/DEBUG
        setTimeout(this.loadTestPage.bind(this))
    }

    static icon() {
        return HomeIcon();
    }
    static label() {
        return "الرئيسية";
    }
    static jump(host: PageWithLink) {
        host.props.navigator.home();
    }

    async loadTestPage() {
        // jumpToKanji(this, "行")
        // jumpToWord(this, "考える")
        // jumpToKanji(this, "愛")
        // jumpToWord(this, "指")
        // jumpToWordSearch(this, "ai");
        // dict.testReadingFinder();
        // console.log(dict.isReadingPossible('置いて', 'おいて'));
    }

    render(): JSX.Element {
        // console.log("user driven search term:", this.state.term);
        let body: JSX.Element = null;
        if(this.state.term) {
            body = <Search term={this.state.term} navigator={this.props.navigator} />
        } else {
            body = <HomePage navigator={this.props.navigator}/>
        }
        return <View style={Styles.pageStyle}>
            <View style={[Styles.navbar]}>
                    <SearchBox hostPage={this}/>
            </View>
                { body }
            </View>
    }
}

export async function search(query: string|number, offset = 0) {
    let anybefore = false;
    let anyafter = false;
    if(typeof query === "string") {
        let term = query as string; // weird TS quirks?!
        if(term.startsWith("@")) {
            let id = parseInt(term.substr(1))
            if(isFinite(id)) {
                query = id;
            }
        } else {
            if (term.startsWith('-')) {
                anybefore = true;
                term = term.substr(1);
            }
            if (term.endsWith('-')) {
                anyafter = true;
                term = term.substring(0, term.length - 1)
            }
            query = term; // ditto weird TS quirks
        }
    }
    return dict.search(query, anybefore, anyafter, offset)
}

export interface SearchProps extends PageProps {
    term: string;
    showNavBar?: boolean; // show back button on search result page
}

export interface SearchState {
    results: dict.SearchResults;
    loading: boolean;
}

export class Search extends React.Component<SearchProps, SearchState> {
    constructor(props: SearchProps) {
        super(props);
        this.state = {
            loading: false, results: null
        };
    }

    componentWillReceiveProps(props: SearchProps) {
        if(!this.mounted) {
            console.warn("New props but not mounted?!");
            return;
        }
        console.log("Getting new props:", props);
        if (!u.isEqual(this.props, props)) {
            this.search(props);
        }
    }

    mounted = false;
    componentWillMount() {
        this.mounted = true;
        this.search(this.props);
    }

    search(props: SearchProps, offset = 0) {
        console.log("Search Term:", props.term);
        if (props.term) {
            let existingWords: dict.WordEntry[] = [];
            if(this.state.results && offset > 0) { // we are appending!
                existingWords = this.state.results.words;
            }
            this.setState({
                loading: true,
                results: this.state.results
            });
            search(props.term, offset).then((results) => {
                // if we already have results, combine exisint results with previous ones
                if(existingWords.length) {
                    results = u.clone(results);
                    results.words = existingWords.concat(results.words);
                }
                this.setState({
                    loading: false,
                    results: results
                })
            })
        } else {
            this.setState({ loading: false, results: null })
        }
    }

    loadNext() {
        if(this.state.results && this.state.results.hasMore) {
            this.search(this.props, this.state.results.nextOffset)
        }
    }

    renderFooter(): JSX.Element {
        if(this.state.results && this.state.results.hasMore) {
            return <View style={Styles.footerView}>
                <TouchableHighlight underlayColor={Styles.underlayColor} style={[Styles.buttonBorder, Styles.buttonShape]} onPress={this.loadNext.bind(this)}>
                    <Text style={{writingDirection: "rtl", textAlign: 'center'}}>بقية النتائج ...</Text>
                </TouchableHighlight>
            </View>
        } else {
            return null
        }
    }

    render() {
        let results: JSX.Element = null;
        if(this.state.results) {
            results = <SearchResults
            entries={this.state.results.words}
            navigator={this.props.navigator}
            showNavBar={this.props.showNavBar}
            renderFooter={this.renderFooter.bind(this)}
             />
        }
        return <View style={{ flex: 1, top: 0, bottom: 0 }}>
        { results }
        </View>
    }
}


export interface EntryListViewProps{
    hostPage: PageWithLink;
    entries: dict.WordEntry[];
    renderFooter?: () => JSX.Element,
    renderHeader?: () => JSX.Element,
}

function EntryListView(props: EntryListViewProps): JSX.Element {
    return <ListView dataSource={wordsDs.cloneWithRows(props.entries) }
        renderRow={(entry) => {
            return <WordEntry key={entry.id} entry={entry} hostPage={props.hostPage} />
        } }
        renderFooter={props.renderFooter}
        renderHeader={props.renderHeader}
        enableEmptySections={true /* suppress warning! */}
        />
}

export interface SearchResultsProps extends PageProps {
    showNavBar?: boolean;
    entries: dict.WordEntry[];
    renderFooter?: () => JSX.Element,
    renderHeader?: () => JSX.Element,
}

export interface SearchResultsState extends PageState {
}

export class SearchResults extends Component<SearchResultsProps, SearchResultsState> {
    constructor(props: SearchResultsProps) {
        super(props)
        this.state = {
            showWorkingModal: false
        }
    }

    render() {
        let props = this.props;
        // console.log("Search results:", props.entries)
        let navbar: JSX.Element = null;
        if (props.showNavBar) {
            navbar = <Navbar hostPage={this} />
        }
        let body: JSX.Element = null;
        if(props.entries.length) {
            body = <EntryListView entries={props.entries}
                renderFooter={this.props.renderFooter}
                renderHeader={this.props.renderHeader}
                hostPage={this} />
        } else {
            body = <View style={Styles.centralMessage}><Text style={Styles.largeWatermark}>لم يتم العثور على كلمة مناسبة</Text></View>
        }

        return <View style={Styles.pageStyle}>
            { navbar }
            { body }
        </View>
    }
}

function icon(src: string, width = 20, height = 20): JSX.Element {
    return <Image source={src} resizeMode="contain" style={{ width, height }}/>
}

function SpeakerIcon() {
    // icon picture from https://www.iconfinder.com/icons/1216557/
    return icon(require('./icons/speaker.png'));
}

function AddIcon() {
    // from https://www.iconfinder.com/icons/728898/
    return icon(require('./icons/add.png'));
}

function RemoveIcon() {
    // from https://www.iconfinder.com/icons/728918
    return icon(require('./icons/cancel.png'));
}

function SearchIcon() {
    // from https://www.iconfinder.com/icons/728952
    return icon(require('./icons/search.png'));
}

function BackIcon() {
    // from https://www.iconfinder.com/icons/126585
    return icon(require('./icons/back.png'));
}

function ReloadIcon() {
    // from https://www.iconfinder.com/icons/326543
    return icon(require('./icons/reload.png'));
}

function ListIcon() {
    // https://www.iconfinder.com/icons/384887
    return icon(require('./icons/list.png'));
}

function DocumentIcon() {
    // https://www.iconfinder.com/icons/728928
    return icon(require('./icons/document.png'));
}

function BookIcon() {
    // https://www.iconfinder.com/icons/728912
    return icon(require('./icons/book.png'));
}

function BubbleIcon() {
    // https://www.iconfinder.com/icons/728956
    return icon(require('./icons/bubble.png'));
}

function ImageIcon() {
    // from the ryoukai website
    return icon(require('./icons/anchor.png'));
}

function CogIcon() {
    // from https://www.iconfinder.com/icons/463013
    return icon(require('./icons/cog.png'));
}

// for the search box!
function CancelIcon() {
    // from https://www.iconfinder.com/icons/216468
    return icon(require('./icons/close.png'), 12, 12);
}

function ForwardIcon(width?: number, height?: number) {
    // from https://www.iconfinder.com/icons/126569
    return icon(require('./icons/forward.png'), width, height);
}

function MoreArrowIcon(width?: number, height?: number) {
    // from https://www.iconfinder.com/icons/476325
    return icon(require('./icons/more-arrow.png'), width, height);
}

function HomeIcon(width?: number, height?: number) {
    // from https://www.iconfinder.com/icons/987495
    return icon(require('./icons/home.png'));
}

let arrA = require('./icons/arrA.png');
let arrS = require('./icons/arrS.png');
let arrB = require('./icons/arrB.png');
let arrM = require('./icons/arrM.png');

export type PageWithLink = Component<PageProps, PageState>;

function showActivityIndicator(page: PageWithLink, yes: boolean) {
    page.props.navigator.showActivityIndicator(yes);
}

async function jumpToWordSearch(view: PageWithLink, word: string|number) {
    console.log("Jumping to word:", word);
    let navigator = view.props.navigator;
    showActivityIndicator(view, true);
    let results = await search(word);
    if(results.words.length === 1) {
        // showWorkingModal(view, false); don't do it here, the jumpToWordPage will do it!
        // if we stop working modal here, there will be an animation glitch
        return await jumpToWordPage(view, results.words[0]);
    } else {
        showActivityIndicator(view, false);
        let props: SearchResultsProps = {
            entries: results.words, navigator, showNavBar: true,
        }
        navigator.push(SearchResults, props)
    }
}

async function jumpToWordPage(view: PageWithLink, entry: dict.WordEntry) {
    showActivityIndicator(view, true);
    let navigator = view.props.navigator;
    let kanjis = await loadWordKanjis(entry);
    let desc = getWordKanji(entry);
    let pictures: dict.WordImageProps[] = await Promise.all(entry.pictures.map(p => dict.loadImageProps(p, desc))) as any; // hacking the broken types!
    // console.log("Pictures:", pictures);
    showActivityIndicator(view, false);
    let props: WordPageProps = { entry, kanjis, pictures, navigator }
    navigator.push(WordPage, props)
}

async function jumpToKanji(view: PageWithLink, pkanji: string) {
    showActivityIndicator(view, true);
    let res = await dict.findKanjiDetails([pkanji]);
    showActivityIndicator(view, false);
    if(res.length == 0) {
        // alert?!
        console.error("Kanji not found!", pkanji);
        // TODO: alert the user!
        return;
    }
    let kanji = res[0];
    jumpToKanjiByObject(view, kanji);
}

function jumpToKanjiByObject(view: PageWithLink, kanji: dict.KanjiDetails) {
    let props: KanjiPageProps = { kanji, navigator: view.props.navigator }
    view.props.navigator.push(KanjiPage, props)
}

/**
 * @param imagePath: relative to app directory
 */
async function jumpToImageByFileName(view: PageWithLink, filename: string) {
    showActivityIndicator(view, true);
    let props = await dict.loadImagePropsByFileName(filename);
    showActivityIndicator(view, false);
    // XXX so apparently our navigator.push method doesn't catch missing keys! the image props is supposed to require a navigator but the method is happy to just let it go! it will not check missing props; just extra ones!
    view.props.navigator.push(WordImagePage, props);
}

interface AddWordButtonProps {
    entry: dict.WordEntry,
    listName?: string,
}

interface AddWordButtonState {
    isAdded: boolean
}

export class AddWordButton extends Component<AddWordButtonProps, AddWordButtonState> {
    constructor(props: AddWordButtonProps) {
        super(props);
        this.state = {
            isAdded: false
        }
        this.updateState();
    }

    private async updateState() {
        let ids = await Preferences.getListWords(this.props.listName)
        let isAdded = u.includes(ids, this.props.entry.id);
        this.setState({ isAdded });
    }

    render() {
        let props = this.props;
        let wordId = props.entry.id
        let listName = props.listName;
        let onPress: () => void;
        let icon: JSX.Element;
        if(this.state.isAdded) {
            icon = RemoveIcon();
            onPress = async () => {
                console.log("Removing word:", wordId);
                await Preferences.removeWordFromList(wordId, listName);
                this.updateState();
            }
        } else {
            icon = AddIcon();
            onPress = async () => {
                console.log("Adding word:", wordId);
                await Preferences.addWordToList(wordId, listName);
                this.updateState();
            }
        }

        return <Button onPress={onPress} iconElement={icon} />
    }
}

export interface WordPageProps extends PageProps{
    entry: dict.WordEntry;
    kanjis: dict.KanjiDetails[];
    pictures: dict.WordImageProps[];
}

export interface WordPageState {
    showWorkingModal: boolean;
    examplesShown: number;
}

async function wait(time: number) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

async function loadWordKanjis(entry: dict.WordEntry) {
    let kanjis: string[] = [];
    let kanjiWords = entry.kanji.map(k => k.kanji);
    for (let kw of kanjiWords) {
        for (let c of kw) {
            if (dict.isCharKanji(c)) {
                kanjis.push(c);
            }
        }
    }
    return await dict.findKanjiDetails(kanjis);
}

export class WordPage extends React.Component<WordPageProps, WordPageState> {
    parsedMeaning: JSX.Element;
    image: JSX.Element;
    constructor(props: WordPageProps) {
        super(props);
        this.state = {
            showWorkingModal: false,
            examplesShown: 3,
        }
        this.parsedMeaning = <View style={{ padding: 10 }}><FullArabicMeaning entry={props.entry} hostPage={this} /></View>
        this.image = <WordImages pictures={props.pictures}/>;
    }

    componentDidMount() {
        if (this.props.entry.soundFileName) {
            this.autoPlaySound();
        }
        console.log("Word page:", this.props.entry.id);
    }

    async autoPlaySound() {
        let yes = await Preferences.getPreference("autoPlaySound");
        if(yes) {
            safePlayAudio(this.props.entry.soundFileName);
        }
    }

    render(): JSX.Element {
        let entry = this.props.entry;
        let audioButton: JSX.Element = null;
        let kanjis = this.props.kanjis;
        if (entry.soundFileName) {
            audioButton = <Button onPress={ safePlayAudio.bind(null, entry.soundFileName) } iconElement={ SpeakerIcon() } />
        }
        let addButton = <AddWordButton entry={entry}/>
        let hostPage: PageWithLink = this;
        let kanjisTitle: JSX.Element = null;
        let kanjisView: JSX.Element = null;
        if (kanjis) {
            kanjisTitle = sectionTitle("漢字 \t الكانجي");
            kanjisView = <View>
                { kanjis.map((k, i) => {
                    return <KanjiSummaryCard key={i} kanji={k} index={i} hostPage={hostPage}/>
                }) }
            </View>
        }

        let examples = entry.examples.slice(0, this.state.examplesShown);
        let examplesTitle = sectionTitle("用例 \t الأمثلة");
        if (examples.length == 0) {
            examplesTitle = null;
        }
        return <View style={Styles.pageStyle}>
            <Navbar hostPage={this} pageButton1={addButton}  pageButton2={audioButton} />
            <ListView dataSource={exampleDs.cloneWithRows(examples) }
                renderRow={(e) => <ExampleSentence example={e} index={examples.indexOf(e)}/>}
                renderHeader={() => {
                    return <View>
                        <View style={{ padding: 10 }}>
                            <JapaneseWord {...entry}/>
                        </View>
                        { this.parsedMeaning }
                        { this.image }
                        { kanjisTitle }
                        { kanjisView }
                        { examplesTitle }
                    </View>
                }}
                renderFooter={() => this.renderFooter()}
                enableEmptySections={true}
                style={{ flex: 1 }}>
            </ListView>
        </View>
    }

    renderFooter(): JSX.Element {
        if(this.state.examplesShown < this.props.entry.examples.length) {
            return <View style={Styles.footerView}>
                <TouchableHighlight underlayColor={Styles.underlayColor} style={[Styles.buttonBorder, Styles.buttonShape]} onPress={this.extendExamples.bind(this)}>
                    <Text style={{writingDirection: "rtl", textAlign: 'center'}}>بقية الأمثلة ...</Text>
                </TouchableHighlight>
            </View>
        } else {
            return null
        }
    }

    extendExamples() {
        let totalExamples = this.props.entry.examples.length;
        let nstate = u.clone(this.state);
        nstate.examplesShown = totalExamples;
        this.setState(nstate);
    }
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

const h2: TextStyle = {
    fontSize: 16, fontWeight: "bold", left: 0, right: 0, textAlign: "center", paddingVertical: 4, paddingTop: 10,
    backgroundColor: Styles.blueishBG, color: Styles.blueishBorder,
}
const h2v: ViewStyle = {
    borderColor: Styles.blueishBorder, borderStyle: "solid", borderTopWidth: 2, borderBottomWidth: 2,
}

function sectionTitle(text: string) {
    return <View style={h2v}><Text style={h2}>{ text }</Text></View>
}

const h2w: TextStyle = {
    backgroundColor: Styles.reddishBG, color: Styles.reddishBorder,
}
const h2vw: ViewStyle = {
    borderColor: Styles.reddishBorder, borderStyle: "solid"
}

function warningTitle(text: string) {
    return <View style={[h2v, h2vw]}><Text style={[h2, h2w]}>{ text }</Text></View>
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------


interface KanjiBaseProps {
    kanji: dict.KanjiDetails;
}

export interface KanjiBaseState {
    animating: boolean; // animating the stroke order diagram
}

class KanjiComponentBase<P extends KanjiBaseProps, S extends KanjiBaseState> extends Component<P, S> {
    toggleAnimation() {
        let state = u.clone(this.state);
        state.animating = !state.animating;
        this.setState(state);
    }

    kanjiImageContent(width = 80, height = 80, fontSize = 70) {
        let {kanji} = this.props;
        if (this.state.animating && kanji.animation) {
            return <Image source={{ uri: kanji.animation.uri }} style={{ height, width }}/>
        } else {
            return <Text style={{ fontSize }}>{kanji.kanji}</Text>
        }
    }
}

export interface KanjiSummaryProps extends KanjiBaseProps {
    index: number;
    hostPage: PageWithLink;
}
export interface KanjiSummaryState extends KanjiBaseState {

}

export class KanjiSummaryCard extends KanjiComponentBase<KanjiSummaryProps, KanjiSummaryState> {
    constructor(props: KanjiSummaryProps) {
        super(props);
        this.state = {
            animating: false,
        }
    }

    jumpToPage() {
        jumpToKanjiByObject(this.props.hostPage, this.props.kanji);
    }

    render() {
        let kanji = this.props.kanji;
        let meaning = kanji.meaning.replace(/\|/g, " | ").replace(/،/g, "، ")
        let kanjiImage = this.kanjiImageContent();
        let styles: ViewStyle[] = [{ flex: 1, flexDirection: 'row', paddingVertical: 10 }];
        if(this.props.index !== 0) {
            styles.push(Styles.borderTop("gray"));
        }
        return <TouchableHighlight underlayColor={Styles.underlayColor}
            onPress={this.jumpToPage.bind(this)}>
            <View style={styles}>
                <TouchableHighlight underlayColor='transparent' onPress={this.toggleAnimation.bind(this) }
                    style={{ width: 80, height: 80, borderRadius: 10, alignItems: "center", justifyContent: "center" }}>
                    { kanjiImage }
                </TouchableHighlight>
                <View style={{ flex: 2, flexDirection: 'column', justifyContent: "space-between", padding: 4 }}>
                    <Text numberOfLines={1} style={{ marginBottom: 6 }}> { kanji.onYomi.join('、') } </Text>
                    <Text numberOfLines={1} style={{ marginBottom: 6 }}> { kanji.kunYomi.join('、') } </Text>
                    <Text numberOfLines={1} style={{ textAlign: "left", writingDirection: "rtl" }}>{rlm}{ meaning }{rlm}</Text>
                </View>
            </View>
        </TouchableHighlight>
    }
}

export interface KanjiPageProps extends PageProps, KanjiBaseProps {
}
export interface KanjiPageState extends KanjiBaseState, PageState {
}

export class KanjiPage extends KanjiComponentBase<KanjiPageProps, KanjiPageState> {
    constructor(props: KanjiPageProps) {
        super(props);
        this.state = {
            animating: true,
        }
        console.log(props.kanji);
    }

    searchWords() {
        jumpToWordSearch(this, "-" + this.props.kanji.kanji + "-");
    }

    static fontSize = 18;

    /// XXX links are not reliable!
    jumpToKunReading(word: string) {
        if(word.indexOf(".") != -1) {
            // place the kanji in its place!!
            word = word.replace(/[ぁ-ん]+\./, this.props.kanji.kanji)
        // } else {
            // NOTE: right now we have now to say: search for word that contains this kanji, or where this kanji is read X
            // word = word.replace(/[ぁ-ん]+/, this.props.kanji.kanji)
        }
        jumpToWordSearch(this, word);
    }

    /// unused because links are not reliable!
    kunLink(kun: string, key: number|string) {
        return <Text key={key} onPress={this.jumpToKunReading.bind(this, kun)}
            style={{ color: "hsl(220, 80%, 40%)", textDecorationLine: "underline", fontSize: KanjiPage.fontSize, marginRight: 20, marginBottom: 10 }}>{ kun }</Text>
    }


    readingPart(r: string, key: number|string) {
        let redStar = <Text style={{ color: Styles.reddishBorder }}>※ </Text>
        return <Text key={key} style={{ fontSize: KanjiPage.fontSize, marginRight: 20, marginBottom: 10 }}>{ redStar }{ r }</Text>
    }

    jumpToKanji(kanji: string) {
        jumpToKanji(this, kanji);
    }

    similarLink(kanji: string, key: number|string) {
        return <Text key={key} onPress={this.jumpToKanji.bind(this, kanji)}
            style={[h2w, { fontSize: 30, marginRight: 20, marginBottom: 10, padding: 10, borderRadius: 10 }]}>{ kanji }</Text>
    }

    readingsv: ViewStyle = {
        left: 0, right: 0,
        padding: 15, flexDirection: "row", flexWrap: "wrap", alignItems: "flex-start"
    }

    render(): JSX.Element {
        let kanji = this.props.kanji;
        let meanings = kanji.meaning.replace(/،/g, "، ").split("|")
        let kanjiImage = this.kanjiImageContent(120, 120, 100);
        let onSection: JSX.Element = null;
        if(kanji.onYomi.length) {
            onSection = <View>
                { sectionTitle("音読み \t القراءة الصينية") }
                <View style={this.readingsv}>
                    { kanji.onYomi.map((r, i) => this.readingPart(r, i)) }
                </View>
            </View>
        }
        let kunSection: JSX.Element = null;
        if(kanji.kunYomi.length) {
            kunSection = <View>
                { sectionTitle("訓読み \t القراءة اليابانية") }
                <View style={this.readingsv}>
                    { kanji.kunYomi.map((s, i) => this.readingPart(s, i)) }
                </View>
            </View>
        }
        let nanoriSection: JSX.Element = null;
        if(kanji.nanori.length) {
            nanoriSection = <View>
                { sectionTitle("名乗り \t في اسماء الأعلام") }
                <View style={this.readingsv}>
                    { kanji.nanori.map((r, i) => this.readingPart(r, i)) }
                </View>
            </View>
        }
        let similarSection: JSX.Element = null;
        if(kanji.similar.length) {
            similarSection = <View>
                { warningTitle("注意 \t تنبيه") }
                <View style={this.readingsv}>
                    { kanji.similar.map((s, i) => this.similarLink(s, i)) }
                </View>
            </View>
        }
        let mnemonicsSection: JSX.Element = null;
        if(kanji.mnemonics.length) {
            mnemonicsSection = <View>
                    { sectionTitle("العبارات التخيلية") }
                    <View style={[this.readingsv, {flexDirection: "column", alignItems: "flex-end"}]}>
                        {
                            kanji.mnemonics.map( (m, i) =>
                                <Text key={i} style={{ textAlign: "right", writingDirection: "rtl", fontSize: KanjiPage.fontSize }}>
                                    { kanji.mnemonics.length > 1 ?
                                    <Text style={Styles.meaningNumber}>({i + 1}) </Text>
                                    : null }
                                    <Text>{rlm}{ m }{rlm}</Text>
                                </Text>
                            )
                        }
                    </View>
            </View>
        }
        return <View style={{ backgroundColor: 'white', flex: 1, top: 0, bottom: 0 }}>
            <Navbar hostPage={this}/>
            <ScrollView>
                <View>
                    <TouchableOpacity onPress={this.toggleAnimation.bind(this) }
                        style={{ left: 0, right: 0, borderRadius: 10, alignItems: "center", justifyContent: "center", padding: 20 }}>
                        { kanjiImage }
                    </TouchableOpacity>
                    <View style={{ margin: 10}}>
                    <TouchableHighlight underlayColor={Styles.underlayColor} onPress={this.searchWords.bind(this)} style={{borderRadius: 4}}>
                        <Text style={{ textAlign: "center", color: "blue", padding: 5 }}> كلمات يدخل هذا الكانجي في تكوينها </Text>
                    </TouchableHighlight>
                    </View>
                    { sectionTitle("意味 \t المعنى") }
                    <View style={[this.readingsv, {flexDirection: "column", alignItems: "flex-end"}]}>
                        {
                            meanings.map( (m, i) =>
                                <Text key={i} style={{ textAlign: "right", writingDirection: "rtl", fontSize: KanjiPage.fontSize }}>
                                    { meanings.length > 1 ?
                                    <Text style={Styles.meaningNumber}>({i + 1}) </Text>
                                    : null }
                                    <Text>{rlm}{ m }{rlm}</Text>
                                </Text>
                            )
                        }
                    </View>
                    { onSection }
                    { kunSection }
                    { nanoriSection }
                    { mnemonicsSection }
                    { similarSection }
                </View>
            </ScrollView>
        </View>
    }
}

import Preferences = require("./user_preferences");

interface PreferencesPageState extends PageState {
    map: Preferences.Map
}

export class PreferencesPage extends Component<PageProps, PreferencesPageState> {
    constructor(props: PageProps) {
        super(props);
        this.state = { map: Preferences.defaults() }
        this.init();
    }

    async init() {
        let map = await Preferences.getAllPreferences();
        this.state.map = map;
        this.setState(this.state)
    }

    static icon() {
        return CogIcon();
    }
    static label() {
        return "إعدادات"
    }
    static jump(host: PageWithLink) {
        jumpToPreferencesPage(host.props.navigator);
    }

    render() {
        let props = this.props;
        let navbar = <View style={[Styles.navbar]}>
            { BackButton(this.props.navigator) }
        </View>
        let containerStyle = [{ borderRadius: 0, marginTop: 10, borderTopWidth: 1, borderTopColor: "silver", borderStyle: "solid" }];
        let optionStyle = [Styles.borderBottom("silver"), {
            paddingVertical: 4, paddingHorizontal: 10,
            flexDirection: "row-reverse", justifyContent: "space-between", alignItems: "center"
        }]
        let optionTextStyle = {
            fontWeight: "bold",
        }
        let sectionTitleStyle: TextStyle = {
            paddingRight: 10, paddingTop: 10, paddingBottom: 4,
            fontWeight: "bold", fontSize: 16, writingDirection: "rtl", textAlign: "right"
        }
        return <View style={Styles.pageStyle}>
            { navbar }
            <ScrollView>
                <View>
                    <Text style={sectionTitleStyle}>إعدادات البرنامج</Text>
                    <View style={containerStyle}>
                        <View style={optionStyle}>
                            <Text style={[optionTextStyle]}>تشغيل الأصوات تلقائياً</Text>
                            <Switch value={this.state.map.autoPlaySound} onValueChange={this.alterBooleanPreference.bind(this, "autoPlaySound")}></Switch>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    }

    async alterBooleanPreference(name: string, value: boolean) {
        await Preferences.setPreference<boolean>(name, value);
        this.state.map[name] = value;
        this.setState(this.state); // HACK!
    }
}

interface HomePageState extends PageState {
}

export class HomePage extends Component<PageProps, HomePageState> {
    constructor(props: PageProps) {
        super(props);
        this.state = {
            showWorkingModal: false
        }
    }


    async jumpToSavedWordsList() {
        jumpToSavedWordsList(this);
    }

    links() {
        return <View style={[Styles.borderBottom("gray"), { flexDirection: "row-reverse", padding: 10 }]}>
            <Button text="إرشادات الاستخدام"/>
        </View>
    }

    render() {
        // console.log("HomePage::Render:", Date.now());
        return <View style={{ flex: 1 }}>
            <View style={{ padding: 20, flex: 1, flexDirection: "row-reverse" }}>
                <Button text="قائمة الكلمات" iconElement={ ListIcon() } onPress={this.jumpToSavedWordsList.bind(this)}/>
                { Spacer() }
            </View>
        </View>
    }
}

function Instructions() {
    return  <View style={{ flexDirection: "row-reverse" }}>
        <Text style={{ color: "gray", textAlign: "right", writingDirection: "rtl" }}>
            يمكن البحث بالعربية او الروماجي او الهيراغانا او الكانجي {'\n'}
        </Text>
    </View>
}

function Spacer() {
    return <View style={{ flex: 1 }}>{ /* spacer*/ }</View>
}

interface NavbarProps {
    hostPage: PageWithLink;
    defaultSearchOn?: boolean;
    pageButton1?: JSX.Element;
    pageButton2?: JSX.Element;
    pageButton3?: JSX.Element;
    disablePrefs?: boolean;
    disableSearch?: boolean;
}

interface NavbarState {
    searchOn: boolean;
}

export class Navbar extends Component<NavbarProps, NavbarState> {
    constructor(props: NavbarProps) {
        super(props);
        this.state = {
            searchOn: Boolean(props.defaultSearchOn)
        }
    }

    toggleSearch(value: boolean = !this.state.searchOn) {
        this.state.searchOn = value;
        this.setState(this.state);
    }

    render() {
        let navigator = this.props.hostPage.props.navigator;
        let back = BackButton(navigator);
        // let prefs = PreferencesButton(navigator);
        let search = <Button iconElement={SearchIcon()} onPress={this.toggleSearch.bind(this)}/>

        let b1 = this.props.pageButton1 || null;
        let b2 = this.props.pageButton2 || null;
        let b3 = this.props.pageButton3 || null;

        let gap = <View style={{width: 10}}/>
        let spacer = Spacer();

        let hostPage = this.props.hostPage;
        if(this.state.searchOn) {
            spacer = <SearchBox hostPage={hostPage} navbar={this} />
            search = null;
            // back = null;
            b1 = null;
            b2 = null;
            b3 = null;
        }

        return <View style={[Styles.navbar]}>
            { back }{ back? gap : null }
            { b1 }{ b1? gap: null }
            { b2 }{ b2? gap: null }
            { b3 }{ b3? gap: null }
            { spacer }
            { search }{ search? gap : null }
            {/* prefs */}
        </View>
    }
}

interface SavedWordsPageProps extends PageProps {
    entries: dict.WordEntry[];
}
interface SavedWordsPageState extends PageState {

}
export class SavedWordListPage extends Component<SavedWordsPageProps, SavedWordsPageState> {
    constructor(props: SavedWordsPageProps) {
        super(props);
        this.state = {
            showWorkingModal: false
        }
    }

    static icon() {
        return ListIcon();
    }
    static label() {
        return "قائمة الكلمات";
    }
    static jump(host: PageWithLink) {
        return jumpToSavedWordsList(host);
    }

    reload() {
        // hack to avoid state; replace this route with a new version of itself!
        jumpToSavedWordsList(this, true);
    }

    render(): JSX.Element {
        let refreshButton = <Button iconElement={ReloadIcon()} onPress={this.reload.bind(this) }/>
        return <View style={Styles.pageStyle}>
            <Navbar hostPage={this} pageButton1={refreshButton}/>
            <EntryListView entries={this.props.entries} hostPage={this} />
        </View>
    }
}

async function jumpToSavedWordsList(view: PageWithLink, replace = false) {
    try {

    let navigator = view.props.navigator;
    showActivityIndicator(view, true)
    let ids = await Preferences.getListWords();
    ids.reverse(); // we want newly added items first!
    let result = await dict.search(ids);
    showActivityIndicator(view, false)
    let entries = u.clone(result.words);
    let props: SavedWordsPageProps = {
        entries, navigator
    }
    if(replace) {
        navigator.replace(SavedWordListPage, props);
    } else {
        navigator.push(SavedWordListPage, props);
    }
    } catch(e) {
        showActivityIndicator(view, false)
        console.log("Error:", e);
    }
}

interface InfoPageProps extends PageProps {

}
interface InfoPageState extends PageState {

}
export class InfoPage extends Component<InfoPageProps, InfoPageState> {
    constructor(props: InfoPageProps) {
        super(props);
        this.state = {
            showWorkingModal: false
        }
    }

    static icon() {
        return BookIcon();
    }
    static label() {
        return "معلومات"
    }
    static jump(host: PageWithLink) {
        host.props.navigator.push(InfoPage, {navigator: host.props.navigator})
    }

    render(): JSX.Element {
        return <View style={Styles.pageStyle}>
            <Navbar hostPage={this} />
            <View style={Styles.centralMessage}>
                <Text style={Styles.largeWatermark}>
                    قيد الإنشاء
                </Text>
            </View>
        </View>
    }
}