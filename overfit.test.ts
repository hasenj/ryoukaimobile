import u = require("lodash");
import overfit = require("./overfit");
const readline = require('readline');

let chunks: number = 0;

// testLine("2147|家にはテレビ三台あります。|うちにはてれべさんだいあります");

declare var process: any;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
let errorLines: string[] = []
rl.on('line', (line: string) => {
    try {
        if(u.trim(line).length) {
            testLine(line);
        }
    } catch(e) {
        errorLines.push(line);
        console.log(line);
        if(e.data) {
            // console.log(e.data);
        } else {
            // console.log(e);
        }
    }
});
rl.on('close', (line: string) => {
    console.log("\n\n");
    console.log("Total error lines:", errorLines.length);
})

function testLine(line: string) {
    let [id, kanji, reading] = line.split("|");
    let result = overfit.overfitToRuby(kanji, reading);
    // console.log(kanji);
    // console.log("<br><br>")
    // console.log(overfit.overfitToRuby(kanji, reading));
    // console.log();
}