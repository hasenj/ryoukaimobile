import { ViewStyle, TextStyle } from "react-native";

export const pageStyle: ViewStyle = {
    flex: 1, backgroundColor: 'white',
}

export const resultJP: TextStyle = {
    fontWeight: 'bold',
    color: 'hsl(240, 30%, 40%)',
    fontSize: 16,
    marginBottom: 10
}

export const resultOutdated: TextStyle = {
    color: 'hsl(240, 10%, 70%)',
}

export const resultAR: TextStyle = {
    writingDirection: 'rtl',
    textAlign: 'right',
    fontSize: 16,
    // fontFamily: 'sans-serif',
}

export let meaningNumber: TextStyle = { color: 'hsl(0, 50%, 60%)', paddingLeft: 10 }

export const buttonBorder: ViewStyle = {
    borderColor: 'hsla(0, 0%, 30%, 0.5)', borderWidth: 0.2,
}
export const buttonShape: ViewStyle = {
    padding: 10, borderRadius: 4, backgroundColor: 'whitesmoke', height: 40,
}
export const underlayColor='hsla(120, 30%, 80%, 0.3)';

export const subtleBorder: ViewStyle = {
    // shadowColor: "black",
    // shadowOffset: { width: 0, height: 0 },
    // shadowOpacity: 0.2,
    shadowRadius: 2,
    borderWidth: 0.2,
    borderColor: "hsla(0, 0%, 0%, 0.15)",
    borderStyle: "solid",
    // backgroundColor: "hsla(0, 0%, 99%, 0.5)",
}

export const navbar: ViewStyle = {
    padding: 10, flexDirection: 'row',
    borderStyle: 'solid',
    borderBottomColor: 'gray', borderBottomWidth: 1,
    borderTopColor: 'silver', borderTopWidth: 1,
}

export const footerView: ViewStyle = {
    borderTopColor: 'silver', borderTopWidth: 1, borderStyle: 'solid',
    padding: 15
}

export const largeWatermark: TextStyle = {
    padding: 60, fontSize: 30, fontWeight: "bold",
    color: "silver", textShadowColor: "whitesmoke",
    textShadowRadius: 2, textShadowOffset: { width: 0, height: 1 },
    textAlign: "center"
}

export const centralMessage: ViewStyle = {
    left: 0, right: 0, justifyContent: "center", alignItems: "center",
}

export function borderBottom(color: string, width = 1): ViewStyle {
    return {
        borderBottomColor: color, borderBottomWidth: width, borderStyle: 'solid'
    }
}

export function borderTop(color: string, width = 1): ViewStyle {
    return {
        borderTopColor: color, borderTopWidth: width, borderStyle: 'solid'
    }
}

export function border(color: string = "blue"): ViewStyle {
    return {
        borderWidth: 1, borderStyle: "solid", borderColor: color
    }
}

export const blueishBG = "hsl(220, 50%, 90%)";
export const blueishBorder = "hsl(220, 80%, 40%)"
export const reddishBG = "hsl(0, 50%, 90%)";
export const reddishBorder = "hsl(0, 80%, 40%)";


export const tabButton: ViewStyle = {
    padding: 5,
    borderRadius: 4,
    justifyContent: "center", alignItems: "center"
}

export const tabButtonActive: ViewStyle = {
    backgroundColor: blueishBG
}

export const tabButtonText: TextStyle = {
    fontSize: 11
}

export const tabButtonActiveText: TextStyle = {
    color: blueishBorder
}