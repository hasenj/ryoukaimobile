import React = require('react');
import ReactNative = require('react-native');

import { MainNavigator } from "./nav"
class RyoukaiMobile extends React.Component<void, void> {
    render(): JSX.Element {
        return <MainNavigator/>
    }
}

ReactNative.AppRegistry.registerComponent('RyoukaiMobile', () => RyoukaiMobile);
