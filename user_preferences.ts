import u = require("lodash");
import { AsyncStorage } from "react-native";

const defaultWordList = "words";
const defaultListType = "word";

type ListName = string;
type WordIDList = number[];

function listKey(listName: ListName, type: string) {
    return `@${ type }list::${ listName }`;
}

export async function getListWords(listName: ListName = defaultWordList, listType = defaultListType): Promise<WordIDList> {
    let result = await AsyncStorage.getItem(listKey(listName, listType));
    if(!result) {
        return [];
    }
    return JSON.parse(result) as WordIDList;
}

export async function addWordToList(wordId: number, listName: ListName = defaultWordList, listType = defaultListType): Promise<void> {
    let list = await getListWords(listName);
    if(list.indexOf(wordId) == -1) {
        list.push(wordId);
    }
    await AsyncStorage.setItem(listKey(listName, listType), JSON.stringify(list));
    notifyListSubscribers(listName, listType);
}

export async function removeWordFromList(wordId: number, listName: ListName = defaultWordList, listType = defaultListType): Promise<void> {
    let list = await getListWords(listName);
    let index = list.indexOf(wordId);
    if(index != -1) {
        list.splice(index, 1);
    }
    await AsyncStorage.setItem(listKey(listName, listType), JSON.stringify(list));
    notifyListSubscribers(listName, listType);
}

export type PreferenceValue = number | boolean | string;
function preferenceKey(pref: string) {
    return `@pref::${ pref }`;
}

export async function setPreference<T extends PreferenceValue>(pref: string, value: T) {
    await AsyncStorage.setItem(preferenceKey(pref), JSON.stringify(value));
}

export async function getPreference<T extends PreferenceValue>(pref: string, def?: T): Promise<T> {
    let result = await AsyncStorage.getItem(preferenceKey(pref));
    if(!result) {
        if(def) {
            return def;
        }
        if(pref in preferenceDefaults) {
            return preferenceDefaults[pref] as T;
        }
    }
    return JSON.parse(result) as T;
}

// NOTE: ALL known preferences must have a default defined here!!
const preferenceDefaults: Map = {
    autoPlaySound: true,
}

export function defaults(): Map {
    return u.clone(preferenceDefaults);
}

export interface Map {
    autoPlaySound: boolean;
    [key: string]: PreferenceValue;
}

export async function getAllPreferences(): Promise<Map> {
    let names = Object.keys(preferenceDefaults);
    let keys = names.map(preferenceKey);
    let result: string[][] = await AsyncStorage.multiGet(keys) as any; // the type definition says Promise<string> but it's actually Promise<string[][]>
    let values = result.map(r => JSON.parse(r[1]) as PreferenceValue);
    return u.zipObject(names, values) as any as Map // what a terrible hack!
}

type fn = () => void;
let subscriptions: { [key: string]: fn[] } = {};
export function listSubscribe(fn: fn, listName = defaultWordList, listType = defaultListType) {
    let key = listKey(listName, listType);
    if(!(key in subscriptions)) {
        subscriptions[key] = [];
    }
    subscriptions[key].push(fn);
}
export function listUnsubscribe(fn: fn, listName = defaultWordList, listType = defaultListType) {
    let key = listKey(listName, listType);
    if(!(key in subscriptions)) {
        return;
    }
    let index = subscriptions[key].indexOf(fn);
    if(index != -1) {
        subscriptions[key].splice(index, 1);
    }
}

function notifyListSubscribers(listName: string, listType: string) {
    console.log("Notifying subscribers to", listType, listName);
    let key = listKey(listName, listType);
    let fns = subscriptions[key];
    if(!fns) {
        return;
    }
    for(let fn of fns) {
        try {
            fn();
        } catch (e) {
            // ignore
        }
    }
}