declare module "chardiff-types" {
    export interface Same {
        type: "=";
        value: string;
    }

    export interface LeftOnly {
        type: "-";
        left: string;
    }

    export interface RightOnly {
        type: "+";
        right: string;
    }

    export interface CharDiff {
        type: "*";
        left: string;
        right: string;
    }

    export type CharResult = Same | LeftOnly | RightOnly | CharDiff;

    export interface LineDiff extends CharDiff {
        diff: CharResult[]
    }

    export type LineResult = Same | LeftOnly | RightOnly | LineDiff;
}

declare module "chardiff" {
    import { LineResult } from "chardiff-types";
    function chardiff(left: string, right: string): LineResult[]
    export = chardiff;
}
