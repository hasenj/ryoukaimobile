declare module 'react-native-fs' {
    /** The absolute path to the main bundle directory */
    const MainBundlePath: string;
    /**  The absolute path to the caches directory */
    const CachesDirectoryPath: string;
    /**  The absolute path to the document directory */
    const DocumentDirectoryPath: string;
    /**  The absolute path to the temporary directory (iOS only) */
    const TemporaryDirectoryPath: string;
    /**  The absolute path to the external, shared directory (android only) */
    const ExternalDirectoryPath: string;

    type ReadDirItem = {
        name: string;     // The name of the item
        path: string;     // The absolute path to the item
        size: string;     // Size in bytes
        isFile: () => boolean;        // Is the file just a file?
        isDirectory: () => boolean;   // Is the file a directory?
    };

    /** Reads the contents of path. This must be an absolute path. */
    function readDir(dirpath: string): Promise<ReadDirItem[]>;

    /** Node.js style version of readDir that returns only the names. Note the lowercase d. */
    function readdir(dirpath: string): Promise<string[]>;

    type StatResult = {
        name: string;     // The name of the item
        path: string;     // The absolute path to the item
        size: string;     // Size in bytes
        mode: number;     // UNIX file mode
        isFile: () => boolean;        // Is the file just a file?
        isDirectory: () => boolean;   // Is the file a directory?
    };

    function stat(filepath: string): Promise<StatResult>;

    type encoding = "utf8" | "ascii" | "base64";

    /**
        Reads the file at path and return contents. encoding can be one of utf8 (default), ascii, base64. Use base64 for reading binary files.

        Note: you will take quite a performance hit if you are reading big files
     */
    function readFile(filepath: string, encoding?: encoding): Promise<string>;

    type WriteFileOptions = {
        // iOS only. See https://developer.apple.com/library/ios/documentation/Cocoa/Reference/Foundation/Classes/NSFileManager_Class/index.html#//apple_ref/doc/constant_group/File_Attribute_Keys
    };

    /** Write the contents to filepath. encoding can be one of utf8 (default), ascii, base64. options optionally takes an object specifying the file's properties, like mode etc. */
    function writeFile(filepath: string, contents: string, encoding?: encoding, options?: WriteFileOptions): Promise<void>;

    /** check if the item exist at filepath. If the item does not exist, return false. */
    function exists(filepath: string): Promise<boolean>;
}