/// based on https://github.com/danrigsby/react-native-web-container
import React = require("react");
import { View, WebView, WebViewProperties, NavState } from "react-native";
import u = require("lodash");

interface WebContainerState {
    height: number;
}

interface WebContainerProps {
    html: string;
    style: string;
}

function wrapHtml(html: string, style: string) {
    return `<div id="wrapper">
        ${html}
    </div>
    <style>
        ${style}
    </style>
    <script>
        var i = 0;
        function updateHeight() {
            var height = document.querySelector("#wrapper").clientHeight;
            height = Math.ceil(height * 1.5); // probably the furigana height
            document.title = height;
            window.location.hash = ++i;
        }
        updateHeight();
        window.addEventListener("load", function() {
            updateHeight();
            setTimeout(updateHeight, 1000);
        });
    window.addEventListener("resize", updateHeight);
    </script>
    `
}

export class WebContainer extends React.Component<WebContainerProps, WebContainerState> {

    onNavigationStateChange(navState: NavState) {
        // console.log("New state!", navState)
        this.setState({
            height: Number(navState.title)
        });
    }

    render() {
        let html = wrapHtml(this.props.html, this.props.style)
        let height = 0;
        if (this.state && this.state.height) {
            height = this.state.height
        }
        return <View>
            <WebView source={{ html: html }} javaScriptEnabled={true} style={{ height: height }}
                onNavigationStateChange={this.onNavigationStateChange.bind(this) }/>
        </View>
    }
}
