import React = require('react');
import { Component } from 'react';
import {
    AppRegistry, Navigator, TouchableHighlight,
    View, ScrollView, Text,
    Modal, ActivityIndicator, Dimensions
} from 'react-native';

// MIT license
import wanakana = require('wanakana');
import dict = require('./dict');
import screens = require('./static_screens')

interface MainProps {
}

interface MainState {
    loading?: boolean
    error?: boolean;
}

import { UserDrivenSearch, PreferencesPage, SavedWordListPage, InfoPage, PageWithLink } from "./components";
import Styles = require("./styles");

export class MainNavigator extends Component<MainProps, MainState> {
    tabbar: TabBar;
    navigator: Navigator;
    active: PageWithLink;
    indicator: OverlayActivityIndicator;
    constructor(props: MainProps) {
        super(props);
        this.state = {
            loading: true,
            error: false,
        }
        this.initDB();
        (window as any).main = this;
    }

    async initDB() {
        try {
            await dict.init();
            this.state.loading = false;
            this.setState(this.state);
        } catch (e) {
            this.state.loading = false;
            this.state.error = true;
            this.setState(this.state);
        }
    }

    back() {
        if(this.navigator.getCurrentRoutes().length > 1) {
            this.navigator.pop();
        }
    }

    home() {
        this.navigator.popToTop();
    }

    push<P>(component: React.ComponentClass<P>, passProps: P) {
        this.navigator.push({ component, passProps })
    }

    replace<P>(component: React.ComponentClass<P>, passProps: P) {
        this.navigator.replace({ component, passProps })
    }

    getCurrentPage() {
        return this.active;
    }

    activeIs(name: string) {
        return this.getActiveName() === name;
    }

    getActiveName(): string {
        return this.active ? this.active.constructor.name : "";
    }

    updateTabBar() {
        setTimeout(() => {
            this.tabbar.setActive(this.getActiveName());
        }, 10)
    }

    showActivityIndicator(yes: boolean) {
        this.indicator.toggle(yes);
    }

    render(): JSX.Element {
        let initialRoute = {
            component: UserDrivenSearch
        }

        return <View style={{ flex: 1 }}>
            <Navigator initialRoute={initialRoute}
                renderScene={(route, navigator) => {
                    this.updateTabBar();
                    return <route.component {...route.passProps} navigator={this} ref={(p: any) => {this.active = p}}/>
                } }
                sceneStyle={{ paddingTop: 25, flex: 1 }}
                ref={(n: any) => { this.navigator = n}} />

            <TabBar navigator={this} ref={(t: any) => this.tabbar = t }/>
            <OverlayActivityIndicator ref={(a: any) => this.indicator = a}/>
        </View>
    }
}

interface TabableComponentClass extends React.ComponentClass<{ navigator: MainNavigator }> {
    icon: () => JSX.Element;
    label: () => string;
    jump: (host: PageWithLink) => void;
}

interface TabBarState {
    activeName: string;
}
interface TabBarProps {
    navigator: MainNavigator
}
class TabBar extends Component<TabBarProps, TabBarState> {
    constructor(props: TabBarProps) {
        super(props);
        this.state = {
            activeName: "UserDrivenSearch"
        }
    }

    setActive(name: string) {
        this.state.activeName = name;
        this.setState(this.state);
    }

    activeIs(name: string) {
        return this.state.activeName == name;
    }

    pageTabButton(page: TabableComponentClass) {
        let activeName = this.state.activeName;
        let isActive = activeName == page.name;
        let icon = page.icon();
        let label = page.label();
        let viewStyle = [Styles.tabButton];
        let textStyle = [Styles.tabButtonText]
        if(isActive) {
            viewStyle.push(Styles.tabButtonActive);
            textStyle.push(Styles.tabButtonActiveText);
        }
        let onPress = () => {
             // hack to keep the button highlighted while the page is loading!
            this.state.activeName = page.name;
            this.setState(this.state);
            // jump to page; could take a while (for loading, etc)
            page.jump(this.props.navigator.getCurrentPage())
        }
        return <TouchableHighlight underlayColor={Styles.underlayColor} onPress={onPress}>
            <View style={viewStyle}>
            { icon }
            <Text style={textStyle}>{ label }</Text>
            </View>
        </TouchableHighlight>
    }

    render() {
        let shadow = {
            shadowColor: Styles.blueishBorder,
            shadowOpacity: 0.2,
            shadowRadius: 1,
            shadowOffset: { width: 0, height: 0 }
        }
        return <View style={[Styles.borderTop(Styles.blueishBorder), shadow, { bottom: 0, height: 56, backgroundColor: "white", flexDirection: "row", justifyContent: "space-between", alignItems: "center", padding: 4}]}>
                { this.pageTabButton(UserDrivenSearch) }
                { this.pageTabButton(SavedWordListPage) }
                { this.pageTabButton(InfoPage) }
                { this.pageTabButton(PreferencesPage) }
            </View>
    }
}

interface ActivityState {
    visible: boolean;
}
export class OverlayActivityIndicator extends Component<void, ActivityState> {
    constructor() {
        super();
        this.state = {
            visible: false
        }
    }
    toggle(yes: boolean = !this.state.visible) {
        this.state.visible = yes;
        this.setState(this.state);
    }
    render() {
        let dims = Dimensions.get("window");
        return <Modal transparent={true} visible={this.state.visible} onRequestClose={() => null}>
                <View style={{ height: 200, top: (dims.height / 2) - 100, width: 200, left: (dims.width / 2) - 100, borderRadius: 20, alignItems: "center", justifyContent: "center", backgroundColor: "hsla(0, 0%, 0%, 0.12)" }}>
                    <ActivityIndicator animating={true} color="black" size="large"/>
                </View>
            </Modal>
    }
}
