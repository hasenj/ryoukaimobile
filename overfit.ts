/** Overfit some kana on top of kanji! */
import u = require("lodash");
import wanakana = require("wanakana");

function normalizeHiragana(mixedText: string) {
    // convert katakana to hiragana
    return mixedText.replace(kt, (s: string) => wanakana.toHiragana(s));
}

function normalizePunctuation(text: string) {
    // replace all punctuation with space, and consolidate consecutive white space into a single space!
    return text.replace(pnc, " ").replace(/\s+/g, " ");
}

interface KanjiReading {
    kanji: string;
    reading: string;
}

export class FuriganaOverfittingError extends Error {
    constructor(public data: any) {
        super("overfitting error");
    }
}

// regexes based on: https://gist.github.com/terrancesnyder/1345094
let kt = /([ァ-ン]+)/g
let hg1_s = "[ぁ-ん]";
let hg1 = new RegExp(hg1_s, 'g');
let kj = /([0-9０-９一-龯々ヶ〇☓×%]+)/g // numbers also could have readings (sometiems ..)
let kjr_s = "[0-9０-９ぁ-ん]" // a possible kanji reading!
let pnc_s = "[、，,{}｛｝。()（）？?!！…「」・]"
let pnc = new RegExp(pnc_s, 'g');
/**
    Returns the reading of only the kanji segments
 */
export function _overfit(pkanji: string, pkana: string): KanjiReading[] {
    // normalize katakana to hiragana in both kanji and kana!!
    let kanji = normalizePunctuation(normalizeHiragana(pkanji));
    let kana = normalizePunctuation(normalizeHiragana(pkana)).replace(/\s/g, ""); // remove spaces from reading
    // console.log(kanji, kana);
    let parts = kanji.match(kj);
    if(!parts) { // there's no kanji at all!
        return [{
            kanji: pkanji, reading: null
        }]
    }
    let regex = new RegExp(
        kanji
        .replace(/\[/, "\\[")
        // // sometimes the spaces are not so consistent, so we need to match more liberally
        // .replace(hg1, (h) => `\\s*${h}\\s*`)
        .replace(/\s+/g, "\\s*")
        .replace(pnc, pnc_s + "?")
        .replace(kj, `(${kjr_s}+)`)

        );
    // console.log(regex);
    // console.log(kana);
    let result = regex.exec(kana);
    if(!result) {
        // console.log("Not normalized enough?")
        // console.log(kanji);
        // console.log(kana);
        throw new FuriganaOverfittingError({pkanji, pkana, kanji, kana, regex, error: "no match"})
    }
    let readings = result.slice(1); // with capture (.*) parts, the first result will always be the whole input
    if (readings.length != parts.length) {
        // use chardiff to try to report errors!!!
        let diff = chardiff(kana, kanji);
        throw new FuriganaOverfittingError({pkanji, pkana, kanji, kana, regex, diff, error: "unequal length"})
        // throw new FuriganaOverfittingError({pkanji, pkana, kanji, kana, regex, error: "unequal length"})
    }
    let out: KanjiReading[] = [];
    for (let i = 0; i < parts.length; i++) {
        out.push({
            kanji: parts[i], reading: readings[i]
        })
    }
    return out;
}

/** Convert kanji text into KanjiReading segments representing kanji and non-kanji parts */
export function overfit(kanji: string, kana: string) {
    let fitting = _overfit(kanji, kana);
    let allParts: KanjiReading[] = [];
    // console.log(fitting);
    while (kanji.length > 0) {
        if (fitting.length == 0) {
            allParts.push({ kanji: kanji, reading: null })
            break;
        }
        let part = fitting.shift();
        let kanjiIndex = kanji.indexOf(part.kanji);
        if (kanjiIndex > 0) {
            let kanaPart = kanji.substring(0, kanjiIndex)
            allParts.push({
                // XXX for non-kanji parts, should the reading be null or the same as kanji?
                kanji: kanaPart, reading: null
            })
        }
        allParts.push(part);
        kanji = kanji.substr(kanjiIndex + part.kanji.length);
    }
    return allParts;
}

export function partsToHtmlRuby(parts: KanjiReading[]): string {
    let out = "";
    for (let p of parts) {
        if (p.reading == null || p.reading === p.kanji) {
            out += `<span class="kana">${ p.kanji }</span>`
        } else {
            out += `<span class="kanji"><ruby>${ p.kanji }<rt>${ p.reading }</rt></ruby></span>`
        }
    }
    return out;
}


import chardiff = require('chardiff');
import CharDiff = require('chardiff-types')
/** Doesn't work as well as the regular overfit but sometimes can be a decent fallback */
export function overfit_by_chardiff(pkanji: string, pkana: string): KanjiReading[] {
    let kanji = normalizeHiragana(pkanji);
    let kana = normalizeHiragana(pkana).replace(/\s/g, ""); // remove spaces from reading
    let diff = chardiff(kanji, kana)[0]; // because it's one line and we only care about the first line!!
    if(diff.type == "=") {
        return [{ kanji, reading: null }]
    }
    if(diff.type == "-" || diff.type == "+") { // could not match anything at all!!
        throw new FuriganaOverfittingError({pkanji, pkana, diff})
    }
    if(diff.type == "*") { // the interesting case
        let problems: CharDiff.CharResult[] = [];
        let out: KanjiReading[] = [];
        let ldiff = (diff as CharDiff.LineDiff)
        for(let d of ldiff.diff) {
            if(d.type == "*") { // again, the interesting case
                let dd = d as CharDiff.CharDiff;
                out.push({
                    kanji: dd.left, reading: dd.right
                })
            } else if(d.type == "=") {
                let dd = d as CharDiff.Same;
                out.push({ kanji: dd.value, reading: null })
            } else  {
                if(d.type == "+") {
                    // something in the kanji line is missing from the kana line!
                    let dd = d as CharDiff.RightOnly;
                    out.push({
                        kanji: dd.right, reading: null
                    })
                    // TODO somehow log problem!!
                }
                let dd = d as any;
                let text = dd.left || dd.right;
                if(text.match(/^(。|\s)$/)) {
                    out.push({ kanji: text, reading: null})
                } else {
                    problems.push(d);
                }
            }
        }
        if(problems.length) {
            throw new FuriganaOverfittingError({diff: ldiff.diff})
        } else {
            return out;
        }
    }
}

export function overfitToRuby(kanji: string, kana: string): string {
    let readings: KanjiReading[];
    try {
        readings = overfit(kanji, kana);
    } catch(e) {
        readings = overfit_by_chardiff(kanji, kana);
    }
    return partsToHtmlRuby(readings);
}