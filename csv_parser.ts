
export class MalformedCSVLine extends Error {

}

export class CSVParser { // more like a tokenizer
    public index = 0; // for iteration
    constructor(public content: string) { }

    readString(): string {
        this.index++; // quote mark
        let start = this.index;
        while (this.index < this.content.length) {
            if (this.content[this.index] == '"') {
                if (this.content[this.index + 1] == '"') {
                    // double double quotes; not end of string
                    this.index += 2;
                    continue;
                } else {
                    // single quote; end of string!
                    let end = this.index;
                    let result = this.content.substring(start, end).replace(/""/g, '"');
                    this.index++; // move to comma or EOL
                    return result;
                }
            }
            this.index++;
        }
        throw new MalformedCSVLine("Line ended abruptly! " + this.content)
    }

    readSegment(): string {
        let start = this.index;
        while (this.index < this.content.length) {
            let chr = this.content[this.index];
            if (chr == ',' || chr == '\n') {
                let end = this.index;
                return this.content.substring(start, end);
                // index already at comma, no need for anything further
            }
            this.index++;
        }
        let end = this.index - 1;
        return this.content.substring(start, end);
    }

    nextLine(): string[] {
        let out: string[] = [];
        while (this.index < this.content.length) {
            if (this.content[this.index] == '"') {
                // look for string end
                out.push(this.readString());
            } else {
                out.push(this.readSegment());
            }
            if (this.content[this.index] == ',') {
                this.index++;
                continue;
            } else if (this.content[this.index] == '\n') {
                this.index++; // done one line! move to next one
                return out;
            } else if (this.index == this.content.length) {
                // done entire file!
                return out;
            } else {
                // What should we do here?
                throw new MalformedCSVLine(`Position ${this.index} expected ',' but found ${this.content[this.index]}. Line: ${this.content}`);
            }
        }
        return out;
    }

    isDone() {
        return !(this.index < this.content.length);
    }
}

export function parseCSV(text: string): string[][] {
    let lines: string[][] = [];
    let parser = new CSVParser(text);
    while (!parser.isDone()) {
        lines.push(parser.nextLine());
    }
    return lines;
}