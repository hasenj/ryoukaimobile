import u = require('lodash');
import SQLite = require('react-native-sqlite-storage')
import { Image, Dimensions } from "react-native";
import Sound = require('react-native-sound');
let { MAIN_BUNDLE } = Sound;

// MIT license
import wanakana = require('wanakana');

export interface KanjiEntry {
    kanji: string;
    outdated: boolean;
}

export interface ExampleEntry {
    id: number,
    text: string;
    reading: string;
    arabic: string;
    difficulty: number;
}

export interface WordEntry {
    id: number;
    kanji: KanjiEntry[];
    kana: string;
    arabicSummary: string[];
    arabicDetails: string[];
    examples: ExampleEntry[];
    soundFileName: string;
    pictures: PictureEntry[];
}

export interface PictureEntry {
    id: number;
    fileName: string;
    description: string;
}

let db: SQLite.Database;


export function isCharKanji(s: string) {
    let code = s.charCodeAt(0);
    return code >= 0x4E00 && code <= 0x9FBF;
}

export function containsKanji(s: string) {
    for (let c of s) {
        if (isCharKanji(c)) {
            return true;
        }
    }
    return false;
}

export function isCharArabic(s: string) {
    let code = s.charCodeAt(0);
    return code >= 0x0600 && code <= 0x06FF;
}

export function containsArabic(s: string) {
    for (let c of s) {
        if (isCharArabic(c)) {
            return true;
        }
    }
    return false;
}


export function init(): Promise<SQLite.Database> {
    return new Promise((resolve, reject) => {
        db = SQLite.openDatabase({ name: 'ryoukai.db', createFromLocation: 1, readOnly: true }, resolve, reject);
    })
}

export function execQuery(sql: string, args: any[]): Promise<SQLite.SqlResultSet> {
    return new Promise((resolve, reject) => {
        // db.open(ok, reject);
        // function ok() {
        db.executeSql(sql, args, resolve, reject);
        // }
    });
}

export function readRows(res: SQLite.SqlResultSet) {
    let out: any[] = [];
    for (let i = 0; i < res.rows.length; i++) {
        out.push(res.rows.item(i));
    }
    return out;
}

export async function getData<T>(sql: string, args: any[] = []): Promise<T[]> {
    // console.info(sql, args);
    let res = await execQuery(sql, args);
    return readRows(res) as T[];
}

export interface SearchResults {
    words: WordEntry[];
    hasMore: boolean;
    nextOffset: number; // if hasMore, what offset to pass to get the start of the next page
}

export async function search(query: string | number | number[], anybefore = false, anyafter = false, offset = 0, length = 20): Promise<SearchResults> {
    let searchField = 'w.Kana';
    let qualifier = 'like';
    let placeholder = '?';
    let arg = query;
    let orderingClause = "order by w.difficulty desc, w.JLPT desc, w.ID asc";
    let orderingIds: number[] = [];
    if (typeof query === "number") {
        searchField = 'w.ID';
        qualifier = '=';
    } else if (query instanceof Array) {
        searchField = 'w.ID';
        qualifier = 'in';
        orderingIds = query.slice(offset, offset + length);
        placeholder = `(${orderingIds.join(',')})`;
        arg = null; // irrelevant!
        orderingClause = ""
    } else if (typeof query === "string") {
        let term = wanakana.toKana(query); // convert romaji to kana
        arg = term;
        if (anybefore) {
            arg = '%' + arg;
        }
        if (anyafter) {
            arg = arg + '%';
        }
        if (containsArabic(term)) {
            searchField = `w.arabicIndex`;
            let argprefix = anybefore ? '%' : '%.';
            let argpostfix = anyafter ? '%' : '.%';
            arg = argprefix + term + argpostfix;
        } else if (containsKanji(term)) {
            searchField = `k.word`
        }
    }

    // console.log("argument:", arg);
    // for safety!
    offset = Number(offset);
    length = Number(length);
    let queryLength = length + 5;

    // Note: by using '?' and passing arg in the second argument, it already does sanitzations against sql injections
    let sql = `select distinct w.ID as id, w.Kana as kana, w.hasKanji as hasKanji, w.arabicIndex as arabicIndex
        from dbo_wordsTable as w
        join dbo_wordsSearch as k on k.wordID = w.ID
        where ${searchField} ${qualifier} ${placeholder}
        ${ orderingClause}
        limit ${queryLength} offset ${offset}`;
    type WordRow = { id: number, kana: string, hasKanji: number, arabicIndex: string, soundFileName: string };
    let args = [arg];
    if(arg === null) { // hack but necessary
        args = [];
    }
    let rows: WordRow[] = await getData<WordRow>(sql, args);
    let wordIds = "(" + rows.map(r => r.id).join(", ") + ")";
    // console.log("Found word ids:", wordIds)
    interface IDMap {
        [id: number]: WordEntry;
    }
    let map: IDMap = {};
    for (let row of rows) {
        map[row.id] = {
            id: row.id, kana: row.kana, arabicSummary: u.compact(row.arabicIndex.split('.')),
            kanji: [], arabicDetails: [], examples: [],
            soundFileName: "", pictures: []
        }
    }

    let meaningSql = `select wordID, arabicMeaning from dbo_wordsTableArabicMeaning where wordID in ${wordIds}`;
    type ArabicRow = { wordID: number, arabicMeaning: string };
    let aRows = await getData<ArabicRow>(meaningSql, []);
    for (let arow of aRows) {
        map[arow.wordID].arabicDetails.push(arow.arabicMeaning);
    }
    let kanjiSql = `select wordID, word as kanji, outdated as outdated from dbo_wordsSearch where wordID in ${wordIds} and onlyForSearch = 0`;
    type KanjiRow = { wordID: number, kanji: string, outdated: string }
    let kRows = await getData<KanjiRow>(kanjiSql, []);
    for (let krow of kRows) {
        // console.log(krow);
        map[krow.wordID].kanji.push({ kanji: krow.kanji, outdated: Boolean(Number(krow.outdated)) });
    }

    // get the sounds
    type SoundRow = { wordID: number, fileName: string }
    let soundSql = `select wordID, fileName from dbo_wordsSounds where wordID in ${wordIds}`;
    let sRows = await getData<SoundRow>(soundSql, []);
    for (let srow of sRows) {
        map[srow.wordID].soundFileName = srow.fileName;
    }

    // get the pictures
    type PictureRow = { wordID: number, picID: number, fileName: string, description: string }
    let picturesSql = `select p.wordID as wordID, p.ID as picID,
        p.picNameInFolder as fileName, p.picDescription as description
        from dbo_wordsPictures as p where wordID in ${wordIds}`;
    let pictureRows = await getData<PictureRow>(picturesSql, []);
    for (let prow of pictureRows) {
        let pentry: PictureEntry = {
            id: prow.picID, fileName: prow.fileName, description: prow.description
        }
        map[prow.wordID].pictures.push(pentry);
    }

    // get the examples!
    type ExampleRow = { wordID: number, exampleID: number, difficulty: number, example: string, kana: string, arabic: string }
    let examplesSql = `select l.wordID as wordID, l.exampleID as exampleID,
        x.example as example, x.kana as kana, x.arabic as arabic, x.difficulty as difficulty
        from dbo_examplesTable as x
        join dbo_examplesToWordsLinks as l on l.exampleID = x.ID
        where l.wordID in ${wordIds}`;
    let xRows = await getData<ExampleRow>(examplesSql, []);
    for (let xrow of xRows) {
        let xentry: ExampleEntry = {
            id: xrow.exampleID,
            text: xrow.example,
            reading: xrow.kana,
            arabic: xrow.arabic,
            difficulty: xrow.difficulty
        }
        map[xrow.wordID].examples.push(xentry);
    }

    let words: WordEntry[];
    if (orderingIds.length) {
        words = orderingIds.map(id => map[id]);
    } else {
        words = rows.map(row => map[row.id]);
    }
    let hasMore = false;
    if (words.length == queryLength) {
        hasMore = true;
        words = words.slice(0, length);
    }
    return {
        words: words,
        hasMore: hasMore,
        nextOffset: offset + words.length
    }
}

export async function getWordSoundFileName(wordId: number): Promise<string> {
    type SoundRow = { fileName: string }
    let rows = await getData<SoundRow>('select fileName from dbo_wordsSounds where wordID = ?', [wordId]);
    if (rows.length) {
        return rows[0].fileName;
    } else {
        return null;
    }
}

export interface ImageProps {
    uri: string;
    width: number;
    height: number;
}

export interface WordImageProps extends ImageProps {
    description: string;
}

export function getImageSize(uri: string) {
    type Size = {width: number; height: number};
    return new Promise<Size>((resolve, reject) => {
        Image.getSize(uri, (width, height) => {
            resolve({width, height})
        }, error => reject(error));
    });
}

export function wordImageUri(fileName: string) {
    return Sound.MAIN_BUNDLE + '/www/wordsPic/' + fileName + '.jpg';
}

export async function loadImageProps(p: PictureEntry, defaultDescription?: string): Promise<WordImageProps> {
    let uri = wordImageUri(p.fileName);
    // console.log("Picture:", p, uri)
    let dims = Dimensions.get("window");
    let width = dims.width;
    // let height = width * p.aspectRatio;
    let height = 0; // will be fixed when we load the image!
    let description = defaultDescription;
    if(p.description) {
        // remove old style links from beginning of description (only exists on a few images)
        description = p.description.replace(/^[0-9#*]+/, "");
    }
    let size = await getImageSize(uri);
    // scale width to fit page!
    let factor = width / size.width;
    // but if the picture's width is smaller then screen width, don't bother!
    if(size.width < width) {
        factor = 1;
    }
    // adjust the height!
    height = Math.floor(factor * size.height);
    return {uri, width, height, description};
}

export async function loadImagePropsByFileName(fileName: string): Promise<WordImageProps> {
    type PictureRow = { wordID: number, picID: number, fileName: string; description: string }
    let picturesSql = `select p.wordID as wordID, p.ID as picID, p.picNameInFolder as fileName,
        p.picWidth as width, p.picHeight as height,
        p.picAspectRatio as aspectRatio, p.picDescription as description
        from dbo_wordsPictures as p where fileName = ?`;
    let pictureRows = await getData<PictureRow>(picturesSql, [fileName]);
    if(pictureRows.length == 0) {
        throw new Error("Image not found");
    }
    if(pictureRows.length > 1) {
        console.warn("Multiple images found!!");
    }
    let row = pictureRows[0];
    // TODO: load description from wordID?
    return loadImageProps({
        id: row.picID, fileName: row.fileName, description: row.description,
    });
}


export async function getKanjiImageProps(frequency: number): Promise<ImageProps> {
    let uri = MAIN_BUNDLE + '/www/kanji/' + frequency + '.gif';
    try {
        let {width, height} = await getImageSize(uri);
        return { uri, width, height }
    } catch(e) {
        return null;
    }
}

export interface KanjiDetails {
    id: number;
    kanji: string;
    meaning: string; // arabic meaning
    similar: string[]; // list of kanji chars
    frequency: number; // also file name! (if not zero)
    animation: ImageProps; // animation file!
    grade: number; // school grade
    JLPT: number; // JLPT level
    isCommon: boolean; // is it jouyou kanji?
    onYomi: string[];
    kunYomi: string[];
    nanori: string[]; // name readings
    radicalNames: string[];
    mnemonics: string[];
    oldVersion?: string; // old version of kanji, if exists!
}

export async function findKanjiDetails(kanjis: string[]): Promise<KanjiDetails[]> {
    // enforce list is of sing char kanjis!
    kanjis = kanjis.filter(k => k.length == 1 && isCharKanji(k));
    // remove duplicates
    kanjis = u.uniq(kanjis);

    interface IDMap {
        [id: number]: KanjiDetails;
    }
    let map: IDMap = {};

    let qparam = "(" + kanjis.map(k => "'" + k + "'").join(",") + ")";
    // note: this query should return one row per kanji!
    let sql = `select k.ID as id, k.Kanji as kanji, k.Frequency as frequency, k.Grade as grade,
        k.commonUse as commonUse, k.newJLPT as JLPT
        from dbo_kanjiTable as k
        where k.kanji in ${qparam}`;
    type KanjiRow = { id: number, kanji: string, frequency: number, grade: number, commonUse: number, JLPT: number, oldKanji: string };
    let krows = await getData<KanjiRow>(sql);

    for (let krow of krows) {
        let entry: KanjiDetails = {
            id: krow.id, kanji: krow.kanji, isCommon: Boolean(krow.commonUse),
            JLPT: krow.JLPT, grade: krow.grade, frequency: krow.frequency, animation: null,
            meaning: "", similar: [], onYomi: [], kunYomi: [], nanori: [], radicalNames: [], mnemonics: []
        }
        map[entry.id] = entry;
    }

    let idsParam = "(" + u.keys(map).join(",") + ")";

    // find readings!
    let reaingsSql = `select r.kanjiID as kanjiId, r.Kun as isKun, r.Class as type, r.Reading as reading from dbo_KanjiReading as r where r.kanjiID in ${idsParam}`;
    type ReadingRow = { kanjiId: number, isKun: string, type: number, reading: string };
    let rrows = await getData<ReadingRow>(reaingsSql);
    for (let rrow of rrows) {
        let entry = map[rrow.kanjiId];
        if (Boolean(Number(rrow.isKun))) {
            if (rrow.type == 0) {
                entry.kunYomi.push(rrow.reading);
            } else if (rrow.type == 1) {
                entry.nanori.push(rrow.reading);
            } else if (rrow.type == 2) {
                // I think this means radical name!
                entry.radicalNames.push(rrow.reading)
            }
        } else {
            entry.onYomi.push(rrow.reading);
        }
    }

    // find meanings!
    let mgsql = `select mg.kanjiID as kanjiId, mg.arabic as meaning from dbo_kanjiArabicMeaning as mg where mg.kanjiID in ${idsParam}`;
    type MeaningRow = { kanjiId: number, meaning: string }
    let mgrows = await getData<MeaningRow>(mgsql);
    for (let mgrow of mgrows) {
        map[mgrow.kanjiId].meaning = mgrow.meaning;
    }

    // find mnemonics
    let mnsql = `select mn.kanjiID as kanjiId, mn.mnemonic as mnemonic from dbo_KanjiMnemonics as mn where mn.kanjiID in ${idsParam}`;
    type MnemonicRow = { kanjiId: number, mnemonic: string };
    let mnrows = await getData<MnemonicRow>(mnsql);
    for (let mnrow of mnrows) {
        map[mnrow.kanjiId].mnemonics.push(mnrow.mnemonic);
    }

    // find similar kanjis
    let smsql = `select sm.dontConfuseWith as similar, sm.origionkanjiID as kanjiId from dbo_kanjiSimilar as sm where kanjiId in ${idsParam}`;
    type SimilarRow = { kanjiId: number, similar: string };
    let smrows = await getData<SimilarRow>(smsql);
    for (let smrow of smrows) {
        map[smrow.kanjiId].similar.push(smrow.similar);
    }

    // find old kanji!
    let oldsql = `select nw.kanjiID as kanjiId, nw.oldKanji as oldKanji from dbo_kanjiNewWriting as nw where kanjiId in ${idsParam}`;
    type OldWritingRow = { kanjiId: number, oldKanji: string }
    let oldrows = await getData<OldWritingRow>(oldsql);
    for (let oldrow of oldrows) {
        map[oldrow.kanjiId].oldVersion = oldrow.oldKanji;
    }

    // find the animation file properties
    for(let id in map) {
        let kanji = map[id];
        if(kanji.frequency) {
            kanji.animation = await getKanjiImageProps(kanji.frequency);
        }
    }

    // sort list to match the order of kanjis in the input parameter
    let kmap: { [k: string]: KanjiDetails } = {};
    for (let id in map) {
        let entry = map[id];
        kmap[entry.kanji] = entry;
    }
    return kanjis.map(k => kmap[k]);
}

// Use the database to find possible readings for a segment of kanji/mixed string
export async function findReadings(kanji: string): Promise<string[]> {
    let sql = `select distinct w.Kana as Reading from dbo_wordsTable as w join dbo_wordsSearch as s on s.wordID = w.ID where s.word = ?;`
    type Row = { Reading: string };
    let readings = await getData<Row>(sql, [kanji]);
    let out = readings.map(r => r.Reading);
    // also grab kun readings!
    if(kanji.length == 1 && isCharKanji(kanji)) {
        sql = `select distinct r.ReadingTrimed as Reading from dbo_KanjiReading as r join dbo_kanjiTable as k on k.ID = r.kanjiID where k.kanji = ? and r.Kun = '1';`
        let readings = await getData<Row>(sql, [kanji]);
        out = u.uniq(out.concat(readings.map(r => r.Reading)));
    }
    return out;
}

export async function nextSegmentReading(text: string): Promise<{ length: number, readings: string[] }> {
    // first, try to find a reading for the whole segment!
    let readings = await findReadings(text);
    if (readings.length) {
        console.log("Found readings:", text, readings);
        // happy case!
        return { length: text.length, readings };
    }
    if (text.length == 1) {
        // saddest case! no reading found! :(
        return { length: 1, readings: [] }
    }
    // Now we want to find the largeest possible subsegment that has a reading
    // We should do some kind of a binary search, but for starters lets do a brute force
    // the O(n) is terrible here!
    return nextSegmentReading(text.substr(0, text.length - 1));
}

export interface SegmentReading {
    text: string;
    readings: string[]
}
export async function computePossibleReadings(text: string): Promise<SegmentReading[]> {
    let out: SegmentReading[] = [];
    let r = await nextSegmentReading(text);
    out.push({
        text: text.substr(0, r.length),
        readings: r.readings
    })
    text = text.substr(r.length);
    if (text.length) {
        out = out.concat(await computePossibleReadings(text))
    }
    return out;
}

export async function testReadingFinder() {
    let computation = await computePossibleReadings("花瓶を棚の上に置きなさい。");
    console.log(computation);
    let repr = computation.map(c => {
        if(c.readings.length == 0) {
            return c.text;
        }
        if(c.readings.length == 1 && c.text == c.readings[0]) {
            return c.text;
        }
        return `{${c.text}:${c.readings.join('|')}}`;
    }).join("");
    console.log(repr);
}

export async function isReadingPossible(kanji: string, reading: string) {
    // first check the word search table
    let [{count}] = await getData<{count: number}>(`select count(*) as count from dbo_wordsTable as w join dbo_wordsSearch as s on s.wordID = w.ID where s.word = ? and w.Kana = ?;`, [kanji, reading]);
    if(count != 0) {
        return true;
    }

    // if that failed, check the kanji readings table
    // TODO!
}